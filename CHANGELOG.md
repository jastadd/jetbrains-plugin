# Changelog

## [Unreleased]

### Added

### Changed

### Deprecated

### Removed

### Fixed

## [0.3.5]

### Added

- Support for IntelliJ IDEA 2024.3.

## [0.3.4] - 2024-08-07

### Added

- Support for IntelliJ IDEA 2024.2.

## [0.3.3] - 2023-12-29

### Added

- Support for IntelliJ IDEA 2023.3.

## [0.3.2] - 2023-09-13

### Added

- Support for IntelliJ IDEA 2023.2.

## [0.3.1]

### Added

- Usages are now highlighted.
- Note that although it works in the Java embeddings within aspects, it does *not* work in "real" Java classes.
  Please use this feature with caution. As with most refactoring tools, it is quite fragile and won't give correct
  results in all cases.
- Aspects and most blocks now can be folded.
- Attributes and inter-type declarations are shown sorted by aspect.
- There are toggle buttons to show or hide attributes, inter-type declarations and rewrites.
- Note that things outside aspects, equations and some other elements are not yet included.
- Support for IntelliJ IDEA 2022.3 and 2023.1.

### Fixed

- A bug for type name refactoring in grammar files which prevented the actual definition from being renamed.
- Wrong inspection messages related to names in wrappers of injected java code ("x", "m", "X").
- A bug that required a visibility modifier in refined constructors.

### Removed

- Support for all IntelliJ IDEA version prior to 2022.3.

## [0.2.0]

### Added

- Support for IntelliJ IDEA 2021.2.3.
- syntax highlighter and color settings
  two file types for jrag and jadd
  embedded java for attribute equation blocks
- configurable highlighters for attribute and api usages
- Dark mode icon which is a bit less colourful.

### Removed

- Support for IntelliJ IDEA 2020.2.4. Minimal version now is 2021.1.1.

[Unreleased]: https://git-st.inf.tu-dresden.de/jastadd/jetbrains-plugin//compare/v0.3.5...HEAD
[0.3.5]: https://git-st.inf.tu-dresden.de/jastadd/jetbrains-plugin//compare/v0.3.4...v0.3.5
[0.3.4]: https://git-st.inf.tu-dresden.de/jastadd/jetbrains-plugin//compare/v0.3.3...v0.3.4
[0.3.3]: https://git-st.inf.tu-dresden.de/jastadd/jetbrains-plugin//compare/v0.3.2...v0.3.3
[0.3.2]: https://git-st.inf.tu-dresden.de/jastadd/jetbrains-plugin//compare/v0.3.1...v0.3.2
[0.3.1]: https://git-st.inf.tu-dresden.de/jastadd/jetbrains-plugin//compare/v0.2.0...v0.3.1
[0.2.0]: https://git-st.inf.tu-dresden.de/jastadd/jetbrains-plugin//commits/v0.2.0
