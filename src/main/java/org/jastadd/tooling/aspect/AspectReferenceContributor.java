package org.jastadd.tooling.aspect;

import com.intellij.openapi.util.TextRange;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.psi.*;
import com.intellij.util.ProcessingContext;
import org.jastadd.tooling.aspect.psi.JastAddAspectAstTypeName;
import org.jastadd.tooling.aspect.psi.JastAddAspectClassOrInterfaceType;
import org.jastadd.tooling.grammar.GrammarReference;
import org.jetbrains.annotations.NotNull;

public class AspectReferenceContributor extends PsiReferenceContributor {

  @Override
  public void registerReferenceProviders(@NotNull PsiReferenceRegistrar registrar) {
    registrar.registerReferenceProvider(PlatformPatterns.psiElement(JastAddAspectAstTypeName.class),
      new PsiReferenceProvider() {
        @NotNull
        @Override
        public PsiReference @NotNull [] getReferencesByElement(@NotNull PsiElement element,
                                                               @NotNull ProcessingContext context) {
          JastAddAspectAstTypeName astTypeName = (JastAddAspectAstTypeName) element;
          String value = astTypeName.getText();
          if (value != null) {
            TextRange range = new TextRange(0, value.length());
            return new PsiReference[]{new GrammarReference(element, range)};
          }
          return PsiReference.EMPTY_ARRAY;
        }
      });
    registrar.registerReferenceProvider(PlatformPatterns.psiElement(JastAddAspectClassOrInterfaceType.class),
      new PsiReferenceProvider() {
        @NotNull
        @Override
        public PsiReference @NotNull [] getReferencesByElement(@NotNull PsiElement element,
                                                               @NotNull ProcessingContext context) {
          JastAddAspectClassOrInterfaceType classOrInterfaceType = (JastAddAspectClassOrInterfaceType) element;
          if (classOrInterfaceType.getTypeArgumentsList().isEmpty() && classOrInterfaceType.getJavaIdentifierList().size() == 1) {
            String value = classOrInterfaceType.getJavaIdentifierList().get(0).getText();
            if (value != null) {
              TextRange range = new TextRange(0, value.length());
              return new PsiReference[]{new GrammarReference(element, range)};
            }
          }
          return PsiReference.EMPTY_ARRAY;
        }
      });
  }

}
