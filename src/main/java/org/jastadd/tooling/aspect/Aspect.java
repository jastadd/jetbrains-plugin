package org.jastadd.tooling.aspect;

public class Aspect extends com.intellij.lang.Language {

  public static final Aspect INSTANCE = new Aspect();

  private Aspect() {
    super("JastAddAspect");
  }

}
