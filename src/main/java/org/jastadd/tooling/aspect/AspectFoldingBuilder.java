package org.jastadd.tooling.aspect;


import com.intellij.lang.ASTNode;
import com.intellij.lang.folding.FoldingBuilderEx;
import com.intellij.lang.folding.FoldingDescriptor;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.FoldingGroup;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiTreeUtil;
import org.jastadd.tooling.aspect.psi.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class AspectFoldingBuilder extends FoldingBuilderEx {

  @Override
  public FoldingDescriptor @NotNull [] buildFoldRegions(@NotNull PsiElement root, @NotNull Document document, boolean quick) {
    List<FoldingDescriptor> descriptors = new ArrayList<>();
    Collection<JastAddAspectAspectDeclaration> aspectDeclarations = PsiTreeUtil.findChildrenOfType(root, JastAddAspectAspectDeclaration.class);
    for (final JastAddAspectAspectDeclaration aspectDeclaration : aspectDeclarations) {
      if (aspectDeclaration.getAspectBody().getTextRange().getLength() > 2) {
        descriptors.add(new FoldingDescriptor(aspectDeclaration.getNode(),
          new TextRange(aspectDeclaration.getAspectBody().getTextRange().getStartOffset() + 1,
            aspectDeclaration.getAspectBody().getTextRange().getEndOffset() - 1)));
      }
    }
    Collection<JastAddAspectClassDeclaration> classDeclarations = PsiTreeUtil.findChildrenOfType(root, JastAddAspectClassDeclaration.class);
    for (final JastAddAspectClassDeclaration classDeclaration : classDeclarations) {
      if (classDeclaration.getClassBody().getTextRange().getLength() > 2) {
        descriptors.add(new FoldingDescriptor(classDeclaration.getNode(),
          new TextRange(classDeclaration.getClassBody().getTextRange().getStartOffset() + 1,
            classDeclaration.getClassBody().getTextRange().getEndOffset() - 1)));
      }
    }
    Collection<JastAddAspectEnumBody> enumBodies = PsiTreeUtil.findChildrenOfType(root, JastAddAspectEnumBody.class);
    for (final JastAddAspectEnumBody enumBody : enumBodies) {
      if (enumBody.getTextRange().getLength() > 2) {
        descriptors.add(new FoldingDescriptor(enumBody.getNode(),
          new TextRange(enumBody.getTextRange().getStartOffset() + 1,
            enumBody.getTextRange().getEndOffset() - 1)));
      }
    }
    Collection<JastAddAspectAnnotationTypeBody> annotationTypeBodies = PsiTreeUtil.findChildrenOfType(root, JastAddAspectAnnotationTypeBody.class);
    for (final JastAddAspectAnnotationTypeBody annotationTypeBody : annotationTypeBodies) {
      if (annotationTypeBody.getTextRange().getLength() > 2) {
        descriptors.add(new FoldingDescriptor(annotationTypeBody.getNode(),
          new TextRange(annotationTypeBody.getTextRange().getStartOffset() + 1,
            annotationTypeBody.getTextRange().getEndOffset() - 1)));
      }
    }
    Collection<JastAddAspectBlock> aspectBlocks = PsiTreeUtil.findChildrenOfType(root, JastAddAspectBlock.class);
    for (final JastAddAspectBlock block : aspectBlocks) {
      if (block.getTextRange().getLength() > 2) {
        descriptors.add(new FoldingDescriptor(block.getNode(),
          new TextRange(block.getTextRange().getStartOffset() + 1,
            block.getTextRange().getEndOffset() - 1)));
      }
    }
    return descriptors.toArray(new FoldingDescriptor[0]);
  }

  @Nullable
  @Override
  public String getPlaceholderText(@NotNull ASTNode node) {
    return " ... ";
  }

  @Override
  public boolean isCollapsedByDefault(@NotNull ASTNode node) {
    return false;
  }

}
