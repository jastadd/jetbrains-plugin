package org.jastadd.tooling.aspect;

import com.intellij.icons.AllIcons;
import com.intellij.ide.projectView.PresentationData;
import com.intellij.ide.structureView.StructureViewTreeElement;
import com.intellij.ide.util.treeView.smartTree.SortableTreeElement;
import com.intellij.ide.util.treeView.smartTree.TreeElement;
import com.intellij.navigation.ItemPresentation;
import com.intellij.psi.NavigatablePsiElement;
import com.intellij.psi.PsiElement;
import com.intellij.psi.util.PsiTreeUtil;
import org.jastadd.tooling.aspect.psi.*;
import org.jastadd.tooling.aspect.psi.impl.*;
import org.jastadd.tooling.util.JastAddIcons;
import org.jetbrains.annotations.NotNull;

import javax.swing.*;
import java.util.Objects;
import java.util.stream.Collectors;

public class AspectStructureViewElement implements StructureViewTreeElement, SortableTreeElement {

  private final NavigatablePsiElement myElement;

  public AspectStructureViewElement(NavigatablePsiElement element) {
    this.myElement = element;
  }

  @Override
  public NavigatablePsiElement getValue() {
    return myElement;
  }

  @Override
  public void navigate(boolean requestFocus) {
    myElement.navigate(requestFocus);
  }

  @Override
  public boolean canNavigate() {
    return myElement.canNavigate();
  }

  @Override
  public boolean canNavigateToSource() {
    return myElement.canNavigateToSource();
  }

  @NotNull
  @Override
  public String getAlphaSortKey() {
    String name = myElement.getName();
    return name != null ? name : "";
  }

  @NotNull
  @Override
  public ItemPresentation getPresentation() {
    ItemPresentation presentation = myElement.getPresentation();

    if (presentation == null) {
      if (myElement instanceof JastAddAspectAspectMethodDeclaration) {
        presentation = new ItemPresentation() {
          @Override
          public String getPresentableText() {
            return AspectStructureViewElement.getPresentableText((JastAddAspectAspectMethodDeclaration) myElement);
          }

          @Override
          public Icon getIcon(boolean unused) {
            return AllIcons.Nodes.Method;
          }
        };
      } else if (myElement instanceof JastAddAspectAspectClassDeclaration) {
        presentation = new ItemPresentation() {
          @Override
          public String getPresentableText() {
            return AspectStructureViewElement.getPresentableText((JastAddAspectAspectClassDeclaration) myElement);
          }

          @Override
          public Icon getIcon(boolean unused) {
            return AllIcons.Nodes.Class;
          }
        };
      } else if (myElement instanceof JastAddAspectAspectEnumDeclaration) {
        presentation = new ItemPresentation() {
          @Override
          public String getPresentableText() {
            return AspectStructureViewElement.getPresentableText((JastAddAspectAspectEnumDeclaration) myElement);
          }

          @Override
          public Icon getIcon(boolean unused) {
            return AllIcons.Nodes.Enum;
          }
        };
      } else if (myElement instanceof JastAddAspectAspectInterfaceDeclaration) {
        presentation = new ItemPresentation() {
          @Override
          public String getPresentableText() {
            return AspectStructureViewElement.getPresentableText((JastAddAspectAspectInterfaceDeclaration) myElement);
          }

          @Override
          public Icon getIcon(boolean unused) {
            return AllIcons.Nodes.Interface;
          }
        };
      } else if (myElement instanceof JastAddAspectAspectFieldDeclaration) {
        presentation = new ItemPresentation() {
          @Override
          public String getPresentableText() {
            return AspectStructureViewElement.getPresentableText((JastAddAspectAspectFieldDeclaration) myElement);
          }

          @Override
          public Icon getIcon(boolean unused) {
            return AllIcons.Nodes.Field;
          }
        };
      } else if (myElement instanceof JastAddAspectAspectConstructorDeclaration) {
        presentation = new ItemPresentation() {
          @Override
          public String getPresentableText() {
            return AspectStructureViewElement.getPresentableText((JastAddAspectAspectConstructorDeclaration) myElement);
          }

          @Override
          public Icon getIcon(boolean unused) {
            return AllIcons.Nodes.Method;
          }
        };
      } else if (myElement instanceof JastAddAspectAspectRewrite) {
        presentation = new ItemPresentation() {
          @Override
          public String getPresentableText() {
            return AspectStructureViewElement.getPresentableText((JastAddAspectAspectRewrite) myElement);
          }

          @Override
          public Icon getIcon(boolean unused) {
            return JastAddIcons.REWRITE;
          }
        };
      }

    }

    return presentation != null ? presentation : new PresentationData();
  }

  private static String getPresentableText(JastAddAspectAspectClassDeclaration decl) {
    return decl.getModifiers().getText()
      + " class " + decl.getClassDeclaration().getSimpleTypeName().getText();
  }

  private static String getPresentableText(JastAddAspectAspectEnumDeclaration decl) {
    return decl.getModifiers().getText()
      + " class " + decl.getSimpleTypeName().getText();
  }

  private static String getPresentableText(JastAddAspectAspectInterfaceDeclaration decl) {
    return decl.getModifiers().getText()
      + " class " + decl.getSimpleTypeName().getText();
  }

  private static String getPresentableText(JastAddAspectAspectFieldDeclaration decl) {
    return decl.getModifiers().getText()
      + " " + decl.getAstTypeName().getText()
      + " : " + decl.getAspectType().getText();
  }

  private static String getPresentableText(JastAddAspectAspectRewrite rewrite) {
    return "rewrite " + rewrite.getAstTypeNameList().get(0).getText()
      + " to " + rewrite.getAspectTypeList().stream().map(JastAddAspectAspectType::getText).collect(Collectors.joining("/"));
  }

  private static String getPresentableText(JastAddAspectAspectConstructorDeclaration decl) {
    return decl.getModifiers().getText()
      + " " + decl.getAstTypeNameList().get(0).getText()
      + "." + decl.getAstTypeNameList().get(1).getText()
      + decl.getFormalParameters().getFormalParameterList().stream()
      .map(JastAddAspectFormalParameter::getType)
      .map(PsiElement::getText).collect(Collectors.joining(", ", "(", ")"));
  }

  private static String getPresentableText(JastAddAspectAspectMethodDeclaration decl) {
    return decl.getModifiers().getText()
      + " " + decl.getAstTypeName().getText()
      + "." + AspectStructureViewElement.getPresentableText(decl.getMethodDeclarator()) + " : "
      + decl.getAspectResultType().getText();
  }

  private static String getPresentableText(JastAddAspectMethodDeclarator declarator) {
    return declarator.getJavaIdentifier().getText()
      + declarator.getFormalParameters().getFormalParameterList().stream()
      .map(JastAddAspectFormalParameter::getType)
      .map(PsiElement::getText).collect(Collectors.joining(", ", "(", ")"));
  }



  @Override
  public TreeElement @NotNull [] getChildren() {
    if (myElement instanceof AspectFile) {
      return PsiTreeUtil.getChildrenOfTypeAsList(myElement, JastAddAspectTypeDeclaration.class)
        .stream()
        .map(JastAddAspectTypeDeclaration::getAspectDeclaration)
        .filter(Objects::nonNull)
        .map(decl -> new AspectStructureViewElement((JastAddAspectAspectDeclarationImpl) decl))
        .toArray(TreeElement[]::new);
    } else if (myElement instanceof JastAddAspectAspectDeclaration) {
      return PsiTreeUtil.getChildrenOfTypeAsList(((JastAddAspectAspectDeclaration) myElement).getAspectBody(), JastAddAspectAspectBodyDeclaration.class)
        .stream()
        .map(decl -> {
          if (decl.getAspectSynAttributeDeclaration() != null) {
            return new AspectStructureViewElement((JastAddAspectAspectSynAttributeDeclarationImpl) decl.getAspectSynAttributeDeclaration());
          } else if (decl.getAspectInhAttributeDeclaration() != null) {
            return new AspectStructureViewElement((JastAddAspectAspectInhAttributeDeclarationImpl) decl.getAspectInhAttributeDeclaration());
          } else if (decl.getCollectionAttribute() != null) {
            return new AspectStructureViewElement((JastAddAspectCollectionAttributeImpl) decl.getCollectionAttribute());
          } else if (decl.getAspectMethodDeclaration() != null) {
            return new AspectStructureViewElement((JastAddAspectAspectMethodDeclarationImpl) decl.getAspectMethodDeclaration());
          } else if (decl.getAspectClassDeclaration() != null) {
            return new AspectStructureViewElement((JastAddAspectAspectClassDeclarationImpl) decl.getAspectClassDeclaration());
          } else if (decl.getAspectEnumDeclaration() != null) {
            return new AspectStructureViewElement((JastAddAspectAspectEnumDeclarationImpl) decl.getAspectEnumDeclaration());
          } else if (decl.getAspectInterfaceDeclaration() != null) {
            return new AspectStructureViewElement((JastAddAspectAspectInterfaceDeclarationImpl) decl.getAspectInterfaceDeclaration());
          } else if (decl.getAspectFieldDeclaration() != null) {
            return new AspectStructureViewElement((JastAddAspectAspectFieldDeclarationImpl) decl.getAspectFieldDeclaration());
          } else if (decl.getAspectConstructorDeclaration() != null) {
            return new AspectStructureViewElement((JastAddAspectAspectConstructorDeclarationImpl) decl.getAspectConstructorDeclaration());
          } else if (decl.getAspectRewrite() != null) {
            return new AspectStructureViewElement((JastAddAspectAspectRewriteImpl) decl.getAspectRewrite());
          }
          // missing cases include:
          // aspect_refine_method_declaration
          // aspect_refine_constructor_declaration
          // aspect_add_interface
          // aspect_extend_interface
          return null;
        })
        .filter(Objects::nonNull)
        .toArray(TreeElement[]::new);
    }

    return EMPTY_ARRAY;
  }

}
