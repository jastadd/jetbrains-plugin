package org.jastadd.tooling.aspect;


import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import org.jastadd.tooling.util.JastAddIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.Map;

public class AspectColorSettingsPage implements ColorSettingsPage {

  private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{
    new AttributesDescriptor("Keyword", AspectSyntaxHighlighter.KEYWORD),
    new AttributesDescriptor("Value-literal (true, false, null)", AspectSyntaxHighlighter.KEYWORD_LITERAL),
    new AttributesDescriptor("Operator", AspectSyntaxHighlighter.OPERATOR),
    new AttributesDescriptor("Comma", AspectSyntaxHighlighter.COMMA),
    new AttributesDescriptor("Parenthesis", AspectSyntaxHighlighter.PARENTHESIS),
    new AttributesDescriptor("Bracket", AspectSyntaxHighlighter.BRACKET),
    new AttributesDescriptor("Brace", AspectSyntaxHighlighter.BRACE),
    new AttributesDescriptor("Identifier", AspectSyntaxHighlighter.IDENTIFIER),
    new AttributesDescriptor("Single-line comment", AspectSyntaxHighlighter.SINGLE_LINE_COMMENT),
    new AttributesDescriptor("Block comment", AspectSyntaxHighlighter.MULTI_LINE_COMMENT),
    new AttributesDescriptor("Doc-comment", AspectSyntaxHighlighter.FORMAL_COMMENT),
    new AttributesDescriptor("Symbol", AspectSyntaxHighlighter.SYMBOL),
    new AttributesDescriptor("Dot", AspectSyntaxHighlighter.DOT),
    new AttributesDescriptor("Semicolon", AspectSyntaxHighlighter.SEMICOLON),
    new AttributesDescriptor("Number", AspectSyntaxHighlighter.NUMBER),
    new AttributesDescriptor("String literal", AspectSyntaxHighlighter.STRING_LITERAL),
    new AttributesDescriptor("Character literal", AspectSyntaxHighlighter.CHARACTER_LITERAL),
    new AttributesDescriptor("Bad value", AspectSyntaxHighlighter.BAD_CHARACTER)
  };

  @Nullable
  @Override
  public Icon getIcon() {
    return JastAddIcons.FILE;
  }

  @NotNull
  @Override
  public SyntaxHighlighter getHighlighter() {
    return new AspectSyntaxHighlighter();
  }

  @NotNull
  @Override
  public String getDemoText() {
    return "coll ArrayList<String> Program.errors();\n" +
      "Div contributes\n" +
      "    \"Division by zero is not allowed!\"\n" +
      "    when getRight().isZero()\n" +
      "    to Program.errors();\n\n" +
      "syn T A.x(int a) circular [bv];\n" +
      "eq A.x(int a) = rv;\n\n" +
      "inh T A.y(int a);\n" +
      "eq C.getA().y(int a) {\n" +
      "  return Java-expr;\n" +
      "}\n";
  }

  @Nullable
  @Override
  public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
    return null;
  }

  @NotNull
  @Override
  public AttributesDescriptor @NotNull [] getAttributeDescriptors() {
    return DESCRIPTORS;
  }

  @NotNull
  @Override
  public ColorDescriptor @NotNull [] getColorDescriptors() {
    return ColorDescriptor.EMPTY_ARRAY;
  }

  @NotNull
  @Override
  public String getDisplayName() {
    return "Aspect";
  }

}
