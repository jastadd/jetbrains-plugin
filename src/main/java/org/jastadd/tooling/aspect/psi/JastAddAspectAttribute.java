package org.jastadd.tooling.aspect.psi;

import com.intellij.psi.PsiElement;
import org.jastadd.tooling.common.psi.NamedElement;

public interface JastAddAspectAttribute extends PsiElement, NamedElement {

  String signature();

}
