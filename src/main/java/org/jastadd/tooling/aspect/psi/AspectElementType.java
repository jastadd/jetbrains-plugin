package org.jastadd.tooling.aspect.psi;

import com.intellij.psi.tree.IElementType;
import org.jastadd.tooling.aspect.Aspect;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class AspectElementType extends IElementType {

  public AspectElementType(@NotNull @NonNls String debugName) {
    super(debugName, Aspect.INSTANCE);
  }

}
