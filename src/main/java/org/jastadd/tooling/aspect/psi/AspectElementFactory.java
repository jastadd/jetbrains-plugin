package org.jastadd.tooling.aspect.psi;

import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFileFactory;
import org.jastadd.tooling.aspect.AspectFileType;

public class AspectElementFactory {

  private AspectElementFactory() {
    throw new IllegalStateException("Utility class");
  }

  public static JastAddAspectAstTypeName createAstTypeName(Project project, String name) {
    final AspectFile file = createFile(project, "aspect A{syn int " + name + ".attributeName();}");
    PsiElement result = file.getFirstChild().findElementAt(17);
    if (result != null) {
      return (JastAddAspectAstTypeName) result.getParent();
    }
    return null;
  }

  public static JastAddAspectClassOrInterfaceType createClassOrInterfaceType(Project project, String name) {
    final AspectFile file = createFile(project, "aspect Navigation{X<" + name + "> X.z=0;}");
    PsiElement result = file.getFirstChild().findElementAt(20);
    if (result != null) {
      return (JastAddAspectClassOrInterfaceType) result.getParent().getParent();
    }
    return null;
  }

  public static AspectFile createFile(Project project, String text) {
    String name = "dummy.jrag";
    return (AspectFile) PsiFileFactory.getInstance(project).
      createFileFromText(name, AspectFileType.INSTANCE, text);
  }

  public static JastAddAspectAspectDeclaration createAspectDeclaration(Project project, String name) {
    final AspectFile file = createFile(project, "aspect " + name + "{}");
    PsiElement result = file.getFirstChild().findElementAt(20);
    if (result != null) {
      return (JastAddAspectAspectDeclaration) result.getParent().getParent();
    }
    return null;
  }
}
