package org.jastadd.tooling.aspect.psi.impl;

import com.intellij.lang.ASTNode;
import com.intellij.navigation.ItemPresentation;
import com.intellij.openapi.util.NlsSafe;
import com.intellij.psi.PsiElement;
import com.intellij.util.IncorrectOperationException;
import org.jastadd.tooling.aspect.psi.AspectTypes;
import org.jastadd.tooling.aspect.psi.JastAddAspectAspectSynAttributeDeclaration;
import org.jastadd.tooling.aspect.psi.JastAddAspectAttribute;
import org.jastadd.tooling.common.psi.impl.NamedElementImpl;
import org.jastadd.tooling.util.JastAddIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.stream.Collectors;

public abstract class JastAddAspectAspectSynAttributeDeclarationImplExtension extends NamedElementImpl implements JastAddAspectAttribute {
  public JastAddAspectAspectSynAttributeDeclarationImplExtension(@NotNull ASTNode node) {
    super(node);
  }


  public String getName() {
    // this finds the *first* ID, which is what we want
    ASTNode nameNode = getNode().findChildByType(AspectTypes.ATTRIBUTE_NAME);
    if (nameNode != null) {
      return nameNode.getText();
    } else {
      return null;
    }
  }

  @Override
  public @Nullable PsiElement getNameIdentifier() {
    return ((JastAddAspectAspectSynAttributeDeclaration) this).getAttributeName();
  }

  @Override
  public PsiElement setName(@NlsSafe @NotNull String name) throws IncorrectOperationException {
    throw new IncorrectOperationException("Renaming collection attributes is not supported.");
  }

  @Override
  public @Nullable PsiElement getIdentifyingElement() {
    return this;
  }

  @Override
  public String signature() {
    JastAddAspectAspectSynAttributeDeclaration decl = (JastAddAspectAspectSynAttributeDeclaration) this;
    return "syn " + decl.getAstTypeName().getText() + "." + decl.getAttributeName().getText() + "(" + decl.getTypeList().stream().map(PsiElement::getText).collect(Collectors.joining(", ")) + ") : " + decl.getAspectType().getText();
  }

  @Override
  public ItemPresentation getPresentation() {
    return new ItemPresentation() {
      @Nullable
      @Override
      public String getPresentableText() {
        return signature();
      }

      @Override
      public Icon getIcon(boolean unused) {
        return JastAddIcons.SYN_ATTRIBUTE;
      }
    };
  }
}
