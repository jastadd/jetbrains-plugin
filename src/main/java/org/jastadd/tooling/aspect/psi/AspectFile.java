package org.jastadd.tooling.aspect.psi;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import org.jastadd.tooling.aspect.Aspect;
import org.jastadd.tooling.aspect.AttributeFileType;
import org.jetbrains.annotations.NotNull;

public class AspectFile extends PsiFileBase {

  public AspectFile(@NotNull FileViewProvider viewProvider) {
    super(viewProvider, Aspect.INSTANCE);
  }

  @NotNull
  @Override
  public FileType getFileType() {
    return AttributeFileType.INSTANCE;
  }

  @Override
  public String toString() {
    return "JastAdd Attribute Aspect File";
  }

}
