package org.jastadd.tooling.aspect.psi.impl;

import com.intellij.extapi.psi.ASTWrapperPsiElement;
import com.intellij.lang.ASTNode;
import com.intellij.psi.*;
import org.jastadd.tooling.aspect.psi.JastAddAspectJavaExtension;
import org.jetbrains.annotations.NotNull;

public class JastAddAspectJavaImplExtension extends ASTWrapperPsiElement implements JastAddAspectJavaExtension {

  public JastAddAspectJavaImplExtension(@NotNull ASTNode node) {
    super(node);
  }

  /**
   * @return {@code true} if this instance can accept injections, {@code false} otherwise
   */
  @Override
  public boolean isValidHost() {
    return true;
  }

  /**
   * Update the host element using the provided text of the injected file. It may be required to escape characters from {@code text}
   * in accordance with the host language syntax. The implementation may delegate to {@link ElementManipulators#handleContentChange(PsiElement, String)}
   * if {@link ElementManipulator} implementation is registered for this element class.
   *
   * @param text text of the injected file
   * @return the updated instance
   */
  @Override
  public PsiLanguageInjectionHost updateText(@NotNull String text) {
    return this;
  }

  /**
   * @return {@link LiteralTextEscaper} instance which will be used to convert the content of this host element to the content of injected file
   */
  @Override
  public @NotNull LiteralTextEscaper<? extends PsiLanguageInjectionHost> createLiteralTextEscaper() {
    return LiteralTextEscaper.createSimple(this);
  }
}
