package org.jastadd.tooling.aspect.psi;

import com.intellij.openapi.util.TextRange;
import com.intellij.psi.AbstractElementManipulator;
import com.intellij.psi.ElementManipulator;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class JastAddAspectClassOrInterfaceTypeManipulator extends AbstractElementManipulator<JastAddAspectClassOrInterfaceType> implements ElementManipulator<JastAddAspectClassOrInterfaceType> {
  /**
   * Changes the element's text to the given new text.
   *
   * @param element    element to be changed
   * @param range      range within the element
   * @param newContent new element text
   * @return changed element
   * @throws IncorrectOperationException if something goes wrong
   */
  @Nullable
  @Override
  public JastAddAspectClassOrInterfaceType handleContentChange(@NotNull JastAddAspectClassOrInterfaceType element, @NotNull TextRange range, String newContent) {
    try {
      if (element.getJavaIdentifierList().size() != 1 || !element.getTypeArgumentsList().isEmpty()) {
        throw new IncorrectOperationException("Operation only valid for \"primitive\" instances of JastAddAspectClassOrInterfaceType");
      }
      return (JastAddAspectClassOrInterfaceType) element.setName(range.replace(element.getText(), newContent));
    } catch (Exception e) { // e.g., in case the range is wrong
      throw new IncorrectOperationException(e);
    }
  }
}
