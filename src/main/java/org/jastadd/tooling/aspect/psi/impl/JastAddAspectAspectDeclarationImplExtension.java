package org.jastadd.tooling.aspect.psi.impl;

import com.intellij.lang.ASTNode;
import com.intellij.navigation.ItemPresentation;
import com.intellij.psi.PsiElement;
import org.jastadd.tooling.aspect.psi.AspectElementFactory;
import org.jastadd.tooling.aspect.psi.JastAddAspectAspectDeclaration;
import org.jastadd.tooling.aspect.psi.JastAddAspectAspectName;
import org.jastadd.tooling.common.psi.NamedElement;
import org.jastadd.tooling.common.psi.impl.NamedElementImpl;
import org.jastadd.tooling.util.JastAddIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class JastAddAspectAspectDeclarationImplExtension extends NamedElementImpl implements NamedElement {

  public JastAddAspectAspectDeclarationImplExtension(@NotNull ASTNode node) {
    super(node);
  }

  public String getName() {
    return getNameIdentifier().getText();
  }

  public PsiElement setName(@NotNull String newName) {
    ASTNode identifierNode = getNameIdentifier().getNode().getFirstChildNode();
    if (identifierNode != null) {
      JastAddAspectAspectDeclaration name = AspectElementFactory.createAspectDeclaration(getProject(), newName);
      assert name != null; // we know the name is not null because we always create the same one
      ASTNode newNameIdentifierNode = name.getAspectName().getNode().getFirstChildNode();
      getNameIdentifier().getNode().replaceChild(identifierNode, newNameIdentifierNode);
    }
    return this;
  }

  @Override
  @NotNull
  public JastAddAspectAspectName getNameIdentifier() {
    return ((JastAddAspectAspectDeclaration) getNode().getPsi()).getAspectName();
  }

  @Override
  public ItemPresentation getPresentation() {
    return new ItemPresentation() {
      @Nullable
      @Override
      public String getPresentableText() {
        return "aspect " + getName();
      }

      @Override
      public Icon getIcon(boolean unused) {
        return JastAddIcons.ASPECT;
      }
    };
  }
}
