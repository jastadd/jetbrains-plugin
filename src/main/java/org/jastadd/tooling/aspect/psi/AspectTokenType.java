package org.jastadd.tooling.aspect.psi;

import com.intellij.psi.tree.IElementType;
import org.jastadd.tooling.aspect.Aspect;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class AspectTokenType extends IElementType {

  public AspectTokenType(@NotNull @NonNls String debugName) {
    super(debugName, Aspect.INSTANCE);
  }

  @Override
  public String toString() {
    return "JastAddAspectTokenType." + super.toString();
  }

}
