package org.jastadd.tooling.aspect.psi.impl;

import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import org.jastadd.tooling.aspect.psi.AspectElementFactory;
import org.jastadd.tooling.aspect.psi.AspectTypes;
import org.jastadd.tooling.aspect.psi.JastAddAspectClassOrInterfaceType;
import org.jastadd.tooling.common.psi.NamedElement;
import org.jastadd.tooling.common.psi.impl.NamedElementImpl;
import org.jetbrains.annotations.NotNull;

public class JastAddAspectClassOrInterfaceTypeImplExtension extends NamedElementImpl implements NamedElement {

  public JastAddAspectClassOrInterfaceTypeImplExtension(@NotNull ASTNode node) {
    super(node);
  }

  public String getName() {
    // this finds the *first* ID, which is what we want
    ASTNode nameNode = getNode().findChildByType(AspectTypes.JAVA_IDENTIFIER);
    if (nameNode != null) {
      return nameNode.getText();
    } else {
      return null;
    }
  }

  public PsiElement setName(@NotNull String newName) {
    ASTNode nameNode = getNode().findChildByType(AspectTypes.JAVA_IDENTIFIER);
    if (nameNode != null) {
      JastAddAspectClassOrInterfaceType name = AspectElementFactory.createClassOrInterfaceType(getProject(), newName);
      assert name != null; // we know the name is not null because we always create the same one
      assert !name.getJavaIdentifierList().isEmpty(); // we know there is always one name - the class name
      ASTNode newKeyNode = name.getJavaIdentifierList().get(0).getNode().getFirstChildNode();
      getNode().replaceChild(nameNode, newKeyNode);
    }
    return this;
  }

  public PsiElement getNameIdentifier() {
    // the entire thing is the identifier
    return getNode().getPsi();
  }

}
