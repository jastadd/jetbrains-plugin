package org.jastadd.tooling.aspect.psi.impl;

import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import org.jastadd.tooling.aspect.psi.AspectElementFactory;
import org.jastadd.tooling.aspect.psi.AspectTypes;
import org.jastadd.tooling.aspect.psi.JastAddAspectAstTypeName;
import org.jastadd.tooling.common.psi.NamedElement;
import org.jastadd.tooling.common.psi.impl.NamedElementImpl;
import org.jetbrains.annotations.NotNull;

public class JastAddAspectAstTypeNameImplExtension extends NamedElementImpl implements NamedElement {

  public JastAddAspectAstTypeNameImplExtension(@NotNull ASTNode node) {
    super(node);
  }

  public String getName() {
    ASTNode nameNode = getNode().findChildByType(AspectTypes.AST_TYPE_NAME);
    if (nameNode != null) {
      return nameNode.getText();
    } else {
      return null;
    }
  }

  public PsiElement setName(@NotNull String newName) {
    ASTNode nameNode = getNode().findChildByType(AspectTypes.AST_TYPE_NAME);
    if (nameNode != null) {
      JastAddAspectAstTypeName name = AspectElementFactory.createAstTypeName(getProject(), newName);
      assert name != null; // we know the name is not null because we always create the same one
      ASTNode newNameNode = name.getNode().getFirstChildNode();
      getNode().replaceChild(nameNode, newNameNode);
    }
    return this;
  }

  public PsiElement getNameIdentifier() {
    return getNode().getPsi();
  }

}
