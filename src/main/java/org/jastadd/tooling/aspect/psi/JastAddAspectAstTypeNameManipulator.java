package org.jastadd.tooling.aspect.psi;

import com.intellij.openapi.util.TextRange;
import com.intellij.psi.AbstractElementManipulator;
import com.intellij.psi.ElementManipulator;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class JastAddAspectAstTypeNameManipulator extends AbstractElementManipulator<JastAddAspectAstTypeName> implements ElementManipulator<JastAddAspectAstTypeName> {
  /**
   * Changes the element's text to the given new text.
   *
   * @param element    element to be changed
   * @param range      range within the element
   * @param newContent new element text
   * @return changed element
   * @throws IncorrectOperationException if something goes wrong
   */
  @Nullable
  @Override
  public JastAddAspectAstTypeName handleContentChange(@NotNull JastAddAspectAstTypeName element, @NotNull TextRange range, String newContent) {
    try {
      return (JastAddAspectAstTypeName) element.setName(range.replace(element.getText(), newContent));
    } catch (Exception e) { // e.g., in case the range is wrong
      throw new IncorrectOperationException(e);
    }
  }
}
