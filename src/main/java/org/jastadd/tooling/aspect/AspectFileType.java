package org.jastadd.tooling.aspect;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jastadd.tooling.util.JastAddIcons;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class AspectFileType extends LanguageFileType {

  public static final AspectFileType INSTANCE = new AspectFileType();

  private AspectFileType() {
    super(Aspect.INSTANCE);
  }

  @NotNull
  @Override
  public String getName() {
    return "JastAdd Aspect";
  }

  @Override
  public @Nls @NotNull String getDisplayName() {
    return getName();
  }

  @NotNull
  @Override
  public String getDescription() {
    return "JastAdd file (for inter-type declarations)";
  }

  @NotNull
  @Override
  public String getDefaultExtension() {
    return "jadd";
  }

  @Nullable
  @Override
  public Icon getIcon() {
    return JastAddIcons.FILE;
  }

}
