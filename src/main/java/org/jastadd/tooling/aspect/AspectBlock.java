package org.jastadd.tooling.aspect;


import com.intellij.formatting.*;
import com.intellij.lang.ASTNode;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiElement;
import com.intellij.psi.TokenType;
import com.intellij.psi.codeStyle.CodeStyleSettings;
import com.intellij.psi.formatter.common.AbstractBlock;
import com.intellij.psi.formatter.common.InjectedLanguageBlockBuilder;
import com.intellij.psi.formatter.java.LeafBlock;
import com.intellij.psi.tree.IElementType;
import org.jastadd.tooling.aspect.psi.*;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class AspectBlock extends AbstractBlock {

  private final SpacingBuilder spacingBuilder;
  private final InjectedLanguageBlockBuilder myInjectedBlockBuilder;

  protected AspectBlock(@NotNull ASTNode node, @Nullable Wrap wrap, @Nullable Alignment alignment,
                        SpacingBuilder spacingBuilder) {
    super(node, wrap, alignment);
    this.spacingBuilder = spacingBuilder;

    myInjectedBlockBuilder = new JavaBlockInjectedBlockBuilder();
  }

  @Override
  protected List<Block> buildChildren() {
    List<Block> blocks = new ArrayList<>();
    if (myNode.getPsi() instanceof JastAddAspectAspectClassDeclaration) {
      myInjectedBlockBuilder.addInjectedBlocks(blocks, myNode, Wrap.createWrap(WrapType.NONE, false), null, Indent.getIndent(Indent.Type.NONE, false, true));
    } else if (myNode.getPsi() instanceof JastAddAspectBlock) {
      myInjectedBlockBuilder.addInjectedBlocks(blocks, myNode, Wrap.createWrap(WrapType.NONE, false), null, Indent.getIndent(Indent.Type.NONE, false, true));
    } else if (myNode.getPsi() instanceof JastAddAspectExpression) {
      myInjectedBlockBuilder.addInjectedBlocks(blocks, myNode, Wrap.createWrap(WrapType.NONE, false), null, Indent.getIndent(Indent.Type.NONE, false, true));
    } else {
      ASTNode child = myNode.getFirstChildNode();
      while (child != null) {
        if (child.getElementType() != TokenType.WHITE_SPACE) {
          Block block = new AspectBlock(child, Wrap.createWrap(WrapType.NONE, false), null, spacingBuilder);
          blocks.add(block);
        }
        child = child.getTreeNext();
      }
    }
    return blocks;
  }

  @Override
  public Indent getIndent() {
    if (myNode.getTreeParent() != null) {
      PsiElement parentPsi = myNode.getTreeParent().getPsi();
      IElementType childType = myNode.getElementType();
      if (parentPsi instanceof JastAddAspectAspectBody && childType != AspectTypes.RBRACE) {
        return Indent.getNormalIndent();
      } else if (parentPsi instanceof JastAddAspectAspectConstructorDeclaration && (myNode.getPsi() instanceof JastAddAspectExplicitConstructorInvocation || myNode.getPsi() instanceof JastAddAspectBlockStatement)) {
        return Indent.getNormalIndent();
      } else if (parentPsi instanceof JastAddAspectCollectionAttribute && (childType == AspectTypes.LBRACKET || childType == AspectTypes.ROOT)) {
        return Indent.getNormalIndent();
      } else if (parentPsi instanceof JastAddAspectCollectionContribution && (childType == AspectTypes.WHEN || childType == AspectTypes.TO || childType == AspectTypes.FOR)) {
        return Indent.getNormalIndent();
      }
      ASTNode nonBlankSuccecssor = myNode.getTreePrev();
      while (nonBlankSuccecssor != null && nonBlankSuccecssor.getElementType() == TokenType.WHITE_SPACE) {
        nonBlankSuccecssor = nonBlankSuccecssor.getTreePrev();
      }
      if (nonBlankSuccecssor != null && nonBlankSuccecssor.getElementType() == AspectTypes.ASSIGN) {
        return Indent.getContinuationIndent();
      }
    }
    return Indent.getNoneIndent();
  }

  @Nullable
  @Override
  public Spacing getSpacing(@Nullable Block child1, @NotNull Block child2) {
    return spacingBuilder.getSpacing(this, child1, child2);
  }

  @Override
  public boolean isLeaf() {
    return myNode.getFirstChildNode() == null;
  }

  @Override
  public boolean isIncomplete() {
    // FIXME this is wrong, but a real fix would require a complete rewrite overriding more methods
    return true;
  }

  private static class AroundBlockBlock extends LeafBlock {
    private final TextRange myRange;

    AroundBlockBlock(ASTNode node, Wrap wrap, Alignment alignment, Indent indent, TextRange range) {
      super(node, wrap, alignment, indent);
      myRange = range;
    }

    @NotNull
    @Override
    public TextRange getTextRange() {
      return myRange;
    }
  }

  private static class JavaBlockInjectedBlockBuilder extends InjectedLanguageBlockBuilder {
    @Override
    public CodeStyleSettings getSettings() {
      return CodeStyleSettings.getDefaults();
    }

    @Override
    public boolean canProcessFragment(String text, ASTNode injectionHost) {
      return true;
    }

    @Override
    public Block createBlockBeforeInjection(ASTNode node, Wrap wrap, Alignment alignment, Indent indent, TextRange range) {
      return new AroundBlockBlock(node, wrap, alignment, indent, range);
    }

    @Override
    public Block createBlockAfterInjection(ASTNode node, Wrap wrap, Alignment alignment, Indent indent, TextRange range) {
      return new AroundBlockBlock(node, wrap, alignment, Indent.getNoneIndent(), range);
    }
  }

}
