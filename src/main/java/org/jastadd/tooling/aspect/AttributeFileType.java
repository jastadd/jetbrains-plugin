package org.jastadd.tooling.aspect;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jastadd.tooling.util.JastAddIcons;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class AttributeFileType extends LanguageFileType {

  public static final AttributeFileType INSTANCE = new AttributeFileType();

  private AttributeFileType() {
    super(Aspect.INSTANCE);
  }

  @NotNull
  @Override
  public String getName() {
    return "JastAdd Attribute Aspect";
  }

  @Override
  public @Nls @NotNull String getDisplayName() {
    return getName();
  }

  @NotNull
  @Override
  public String getDescription() {
    return "JastAdd file (for attributes)";
  }

  @NotNull
  @Override
  public String getDefaultExtension() {
    return "jrag";
  }

  @Nullable
  @Override
  public Icon getIcon() {
    return JastAddIcons.FILE;
  }

}
