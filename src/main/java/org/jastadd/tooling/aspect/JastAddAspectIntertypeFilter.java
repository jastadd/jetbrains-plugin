package org.jastadd.tooling.aspect;

import com.intellij.ide.util.treeView.smartTree.ActionPresentation;
import com.intellij.ide.util.treeView.smartTree.ActionPresentationData;
import com.intellij.ide.util.treeView.smartTree.Filter;
import com.intellij.ide.util.treeView.smartTree.TreeElement;
import org.jastadd.tooling.aspect.psi.JastAddAspectAspectEnumDeclaration;
import org.jastadd.tooling.aspect.psi.JastAddAspectAspectFieldDeclaration;
import org.jastadd.tooling.aspect.psi.JastAddAspectAspectInterfaceDeclaration;
import org.jastadd.tooling.aspect.psi.JastAddAspectAspectMethodDeclaration;
import org.jastadd.tooling.aspect.psi.impl.JastAddAspectAspectClassDeclarationImpl;
import org.jastadd.tooling.util.JastAddIcons;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class JastAddAspectIntertypeFilter implements Filter {
  @NonNls
  public static final String ID = "HIDE_INTERTYPE_DECLS";

  @Override
  public boolean isVisible(TreeElement treeNode) {
    if (treeNode instanceof AspectStructureViewElement) {
      return !(
        ((AspectStructureViewElement) treeNode).getValue() instanceof JastAddAspectAspectMethodDeclaration
          || ((AspectStructureViewElement) treeNode).getValue() instanceof JastAddAspectAspectClassDeclarationImpl
          || ((AspectStructureViewElement) treeNode).getValue() instanceof JastAddAspectAspectEnumDeclaration
          || ((AspectStructureViewElement) treeNode).getValue() instanceof JastAddAspectAspectInterfaceDeclaration
          || ((AspectStructureViewElement) treeNode).getValue() instanceof JastAddAspectAspectFieldDeclaration
      );
    } else {
      return true;
    }
  }

  @Override
  @NotNull
  public ActionPresentation getPresentation() {
    // TODO use i18n and string bundle like JavaStructureViewBundle
    return new ActionPresentationData("Hide Intertype Declarations", null, JastAddIcons.INTERTYPE_DECL);
  }

  @Override
  @NotNull
  public String getName() {
    return ID;
  }

  @Override
  public boolean isReverted() {
    return false;
  }
}
