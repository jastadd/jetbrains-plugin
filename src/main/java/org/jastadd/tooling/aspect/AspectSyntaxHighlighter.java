package org.jastadd.tooling.aspect;


import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.HighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import org.jastadd.tooling.aspect.lexer.AspectLexerAdapter;
import org.jastadd.tooling.aspect.psi.AspectTypes;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

public class AspectSyntaxHighlighter extends SyntaxHighlighterBase {

  private static final List<IElementType> javaKeywords = Arrays.asList(
    AspectTypes.ABSTRACT, AspectTypes.BOOLEAN, AspectTypes.BYTE, AspectTypes.CHAR, AspectTypes.DOUBLE,
    AspectTypes.EXTENDS, AspectTypes.FINAL, AspectTypes.FLOAT, AspectTypes.FOR, AspectTypes.IMPLEMENTS,
    AspectTypes.IMPORT, AspectTypes.INSTANCEOF, AspectTypes.INT, AspectTypes.INTERFACE, AspectTypes.LONG,
    AspectTypes.NATIVE, AspectTypes.NEW, AspectTypes.PRIVATE, AspectTypes.PROTECTED, AspectTypes.PUBLIC,
    AspectTypes.SHORT, AspectTypes.STATIC, AspectTypes.STRICTFP, AspectTypes.SUPER, AspectTypes.SYNCHRONIZED,
    AspectTypes.THIS, AspectTypes.THROWS, AspectTypes.TRANSIENT, AspectTypes.VOID, AspectTypes.VOLATILE);
  // some java keywords are missing since they are not parsed by us
  // assert break case catch class continue default do else enum finally if package return switch throw try while

  private static final List<IElementType> jastAddKeywords = Arrays.asList(AspectTypes.ASPECT,
    AspectTypes.NTA, AspectTypes.CIRCULAR, AspectTypes.CONTRIBUTES,
    AspectTypes.COLL, AspectTypes.CACHE, AspectTypes.EQUATION,
    AspectTypes.INH, AspectTypes.SYN,
    AspectTypes.UNCACHE, AspectTypes.REFINE, AspectTypes.REWRITE,
    AspectTypes.TO, AspectTypes.WHEN, AspectTypes.WITH,
    AspectTypes.ROOT, AspectTypes.LAZY, AspectTypes.EACH);
  public static final TextAttributesKey KEYWORD = createTextAttributesKey("JASTADD_KEYWORD", DefaultLanguageHighlighterColors.KEYWORD);
  private static final TextAttributesKey[] KEYWORD_KEYS = new TextAttributesKey[]{KEYWORD};

  private static final List<IElementType> keywordLiterals = Arrays.asList(AspectTypes.TRUE, AspectTypes.FALSE,
    AspectTypes.NULL);
  public static final TextAttributesKey KEYWORD_LITERAL = createTextAttributesKey("ASPECT_KEYWORD", DefaultLanguageHighlighterColors.PREDEFINED_SYMBOL);
  private static final TextAttributesKey[] KEYWORD_LITERAL_KEYS = new TextAttributesKey[]{KEYWORD_LITERAL};

  private static final List<IElementType> operators = Arrays.asList(AspectTypes.ANDASSIGN, AspectTypes.ASSIGN,
    AspectTypes.BIT_AND, AspectTypes.BIT_OR, AspectTypes.DECR, AspectTypes.EQ, AspectTypes.GE, AspectTypes.GT,
    AspectTypes.INCR, AspectTypes.LE, AspectTypes.LT, AspectTypes.LSHIFT, AspectTypes.LSHIFTASSIGN, AspectTypes.PLUS,
    AspectTypes.PLUSASSIGN, AspectTypes.MINUS, AspectTypes.MINUSASSIGN, AspectTypes.SLASH, AspectTypes.SLASHASSIGN,
    AspectTypes.NE, AspectTypes.ORASSIGN, AspectTypes.XOR, AspectTypes.XORASSIGN, AspectTypes.STAR,
    AspectTypes.STARASSIGN, AspectTypes.REM, AspectTypes.REMASSIGN, AspectTypes.SC_AND, AspectTypes.SC_OR,
    AspectTypes.AMPERSAND, AspectTypes.RSIGNEDSHIFTASSIGN, AspectTypes.RUNSIGNEDSHIFTASSIGN, AspectTypes.TILDE,
    AspectTypes.BANG, AspectTypes.ARROW, AspectTypes.ELLIPSIS);
  public static final TextAttributesKey OPERATOR = createTextAttributesKey("JASTADD_OPERATOR", DefaultLanguageHighlighterColors.OPERATION_SIGN);
  private static final TextAttributesKey[] OPERATOR_KEYS = new TextAttributesKey[]{OPERATOR};

  public static final TextAttributesKey COMMA = createTextAttributesKey("JASTADD_COMMA", DefaultLanguageHighlighterColors.COMMA);
  private static final TextAttributesKey[] COMMA_KEYS = new TextAttributesKey[]{COMMA};

  private static final List<IElementType> parentheses = Arrays.asList(AspectTypes.LPAREN, AspectTypes.RPAREN);
  public static final TextAttributesKey PARENTHESIS = createTextAttributesKey("JASTADD_PARENTHESIS", DefaultLanguageHighlighterColors.PARENTHESES);
  private static final TextAttributesKey[] PARENTHESIS_KEYS = new TextAttributesKey[]{PARENTHESIS};

  private static final List<IElementType> brackets = Arrays.asList(AspectTypes.LBRACKET, AspectTypes.RBRACKET);
  public static final TextAttributesKey BRACKET = createTextAttributesKey("JASTADD_BRACKET", DefaultLanguageHighlighterColors.BRACKETS);
  private static final TextAttributesKey[] BRACKET_KEYS = new TextAttributesKey[]{BRACKET};

  private static final List<IElementType> braces = Arrays.asList(AspectTypes.LBRACE, AspectTypes.RBRACE);
  public static final TextAttributesKey BRACE = createTextAttributesKey("JASTADD_BRACE", DefaultLanguageHighlighterColors.BRACES);
  private static final TextAttributesKey[] BRACE_KEYS = new TextAttributesKey[]{BRACE};

  public static final TextAttributesKey IDENTIFIER = createTextAttributesKey("JASTADD_IDENTIFIER", DefaultLanguageHighlighterColors.IDENTIFIER);
  private static final TextAttributesKey[] IDENTIFIER_KEYS = new TextAttributesKey[]{IDENTIFIER};

  public static final TextAttributesKey FORMAL_COMMENT = createTextAttributesKey("JASTADD_FORMAL_COMMENT", DefaultLanguageHighlighterColors.DOC_COMMENT);
  private static final TextAttributesKey[] FORMAL_COMMENT_KEYS = new TextAttributesKey[]{FORMAL_COMMENT};
  public static final TextAttributesKey SINGLE_LINE_COMMENT = createTextAttributesKey("JASTADD_SINGLE_LINE_COMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT);
  private static final TextAttributesKey[] SINGLE_LINE_COMMENT_KEYS = new TextAttributesKey[]{SINGLE_LINE_COMMENT};
  public static final TextAttributesKey MULTI_LINE_COMMENT = createTextAttributesKey("JASTADD_MULTI_LINE_COMMENT", DefaultLanguageHighlighterColors.BLOCK_COMMENT);
  private static final TextAttributesKey[] MULTI_LINE_COMMENT_KEYS = new TextAttributesKey[]{MULTI_LINE_COMMENT};

  // other symbols
  private static final List<IElementType> symbols = Arrays.asList(AspectTypes.DOUBLECOLON, AspectTypes.QUESTIONMARK, AspectTypes.COLON, AspectTypes.DOUBLECOLON);
  public static final TextAttributesKey SYMBOL = createTextAttributesKey("JASTADD_SYMBOL", DefaultLanguageHighlighterColors.PREDEFINED_SYMBOL);
  private static final TextAttributesKey[] SYMBOL_KEYS = new TextAttributesKey[]{SYMBOL};

  public static final TextAttributesKey DOT = createTextAttributesKey("JASTADD_DOT", DefaultLanguageHighlighterColors.DOT);
  private static final TextAttributesKey[] DOT_KEYS = new TextAttributesKey[]{DOT};

  public static final TextAttributesKey SEMICOLON = createTextAttributesKey("JASTADD_SEMICOLON", DefaultLanguageHighlighterColors.SEMICOLON);
  private static final TextAttributesKey[] SEMICOLON_KEYS = new TextAttributesKey[]{SEMICOLON};

  private static final List<IElementType> numbers = Arrays.asList(AspectTypes.INTEGER_LITERAL, AspectTypes.FLOATING_POINT_LITERAL);
  public static final TextAttributesKey NUMBER = createTextAttributesKey("ASPECT_NUMBER", DefaultLanguageHighlighterColors.NUMBER);
  private static final TextAttributesKey[] NUMBER_KEYS = new TextAttributesKey[]{NUMBER};

  public static final TextAttributesKey STRING_LITERAL = createTextAttributesKey("ASPECT_STRING_LITERAL", DefaultLanguageHighlighterColors.STRING);
  private static final TextAttributesKey[] STRING_LITERAL_KEYS = new TextAttributesKey[]{STRING_LITERAL};
  public static final TextAttributesKey CHARACTER_LITERAL = createTextAttributesKey("ASPECT_CHARACTER_LITERAL", DefaultLanguageHighlighterColors.STRING);
  private static final TextAttributesKey[] CHARACTER_LITERAL_KEYS = new TextAttributesKey[]{CHARACTER_LITERAL};

  // unclear
  // TODO unclear IO
  // TODO missing AT_NAME
  // TODO combined DOT_CLASS

  public static final TextAttributesKey BAD_CHARACTER =
    createTextAttributesKey("JASTADD_BAD_CHARACTER", HighlighterColors.BAD_CHARACTER);


  @NotNull
  @Override
  public Lexer getHighlightingLexer() {
    return new AspectLexerAdapter();
  }

  @NotNull
  @Override
  public TextAttributesKey @NotNull [] getTokenHighlights(IElementType tokenType) {
    if (javaKeywords.contains(tokenType) || jastAddKeywords.contains(tokenType)) {
      return KEYWORD_KEYS;
    } else if (keywordLiterals.contains(tokenType)) {
      return KEYWORD_LITERAL_KEYS;
    } else if (operators.contains(tokenType)) {
      return OPERATOR_KEYS;
    } else if (tokenType.equals(AspectTypes.COMMA)) {
      return COMMA_KEYS;
    } else if (parentheses.contains(tokenType)) {
      return PARENTHESIS_KEYS;
    } else if (brackets.contains(tokenType)) {
      return BRACKET_KEYS;
    } else if (braces.contains(tokenType)) {
      return BRACE_KEYS;
    } else if (numbers.contains(tokenType)) {
      return NUMBER_KEYS;
    } else if (symbols.contains(tokenType)) {
      return SYMBOL_KEYS;
    } else if (tokenType.equals(AspectTypes.IDENTIFIER)) {
      return IDENTIFIER_KEYS;
    } else if (tokenType.equals(AspectTypes.FORMAL_COMMENT)) {
      return FORMAL_COMMENT_KEYS;
    } else if (tokenType.equals(AspectTypes.SINGLE_LINE_COMMENT)) {
      return SINGLE_LINE_COMMENT_KEYS;
    } else if (tokenType.equals(AspectTypes.MULTI_LINE_COMMENT)) {
      return MULTI_LINE_COMMENT_KEYS;
    } else if (tokenType.equals(AspectTypes.DOT)) {
      return DOT_KEYS;
    } else if (tokenType.equals(AspectTypes.SEMICOLON)) {
      return SEMICOLON_KEYS;
    } else if (tokenType.equals(AspectTypes.STRING_LITERAL)) {
      return STRING_LITERAL_KEYS;
    } else if (tokenType.equals(AspectTypes.CHARACTER_LITERAL)) {
      return CHARACTER_LITERAL_KEYS;
    } else if (tokenType.equals(TokenType.BAD_CHARACTER)) {
      return new TextAttributesKey[]{BAD_CHARACTER};
    } else {
      return new TextAttributesKey[0];
    }
  }

}
