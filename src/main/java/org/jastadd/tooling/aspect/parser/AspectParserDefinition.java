package org.jastadd.tooling.aspect.parser;

import com.intellij.lang.ASTNode;
import com.intellij.lang.ParserDefinition;
import com.intellij.lang.PsiParser;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.project.Project;
import com.intellij.psi.FileViewProvider;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IFileElementType;
import com.intellij.psi.tree.TokenSet;
import org.jastadd.tooling.aspect.Aspect;
import org.jastadd.tooling.aspect.lexer.AspectLexerAdapter;
import org.jastadd.tooling.aspect.psi.AspectFile;
import org.jastadd.tooling.aspect.psi.AspectTypes;
import org.jetbrains.annotations.NotNull;

public class AspectParserDefinition implements ParserDefinition {

  public static final TokenSet WHITE_SPACES = TokenSet.create(TokenType.WHITE_SPACE);
  public static final TokenSet COMMENTS = TokenSet.create(AspectTypes.SINGLE_LINE_COMMENT, AspectTypes.MULTI_LINE_COMMENT, AspectTypes.FORMAL_COMMENT);
  public static final TokenSet STRING_LITERALS = TokenSet.create(AspectTypes.STRING_LITERAL);

  public static final IFileElementType FILE = new IFileElementType(Aspect.INSTANCE);

  @NotNull
  @Override
  public Lexer createLexer(Project project) {
    return new AspectLexerAdapter();
  }

  @NotNull
  @Override
  public TokenSet getWhitespaceTokens() {
    return WHITE_SPACES;
  }

  @NotNull
  @Override
  public TokenSet getCommentTokens() {
    return COMMENTS;
  }

  @NotNull
  @Override
  public TokenSet getStringLiteralElements() {
    return STRING_LITERALS;
  }

  @NotNull
  @Override
  public PsiParser createParser(final Project project) {
    return new JastAddAspectParser();
  }

  @Override
  public @NotNull IFileElementType getFileNodeType() {
    return FILE;
  }

  @Override
  public @NotNull PsiFile createFile(@NotNull FileViewProvider viewProvider) {
    return new AspectFile(viewProvider);
  }

  @Override
  public @NotNull SpaceRequirements spaceExistenceTypeBetweenTokens(ASTNode left, ASTNode right) {
    return SpaceRequirements.MAY;
  }

  @NotNull
  @Override
  public PsiElement createElement(ASTNode node) {
    return AspectTypes.Factory.createElement(node);
  }

}
