package org.jastadd.tooling.aspect;

import com.intellij.ide.util.treeView.smartTree.ActionPresentation;
import com.intellij.ide.util.treeView.smartTree.ActionPresentationData;
import com.intellij.ide.util.treeView.smartTree.Filter;
import com.intellij.ide.util.treeView.smartTree.TreeElement;
import org.jastadd.tooling.aspect.psi.JastAddAspectAspectRewrite;
import org.jastadd.tooling.aspect.psi.JastAddAspectAttribute;
import org.jastadd.tooling.util.JastAddIcons;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class JastAddAspectRewriteFilter implements Filter {
  @NonNls
  public static final String ID = "HIDE_REWRITES";

  @Override
  public boolean isVisible(TreeElement treeNode) {
    if (treeNode instanceof AspectStructureViewElement) {
      return !(((AspectStructureViewElement) treeNode).getValue() instanceof JastAddAspectAspectRewrite);
    }
    else {
      return true;
    }
  }

  @Override
  @NotNull
  public ActionPresentation getPresentation() {
    // TODO use i18n and string bundle like JavaStructureViewBundle
    return new ActionPresentationData("Hide Rewrites", null, JastAddIcons.REWRITE);
  }

  @Override
  @NotNull
  public String getName() {
    return ID;
  }

  @Override
  public boolean isReverted() {
    return false;
  }
}
