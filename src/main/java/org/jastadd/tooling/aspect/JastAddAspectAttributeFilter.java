package org.jastadd.tooling.aspect;

import com.intellij.ide.util.treeView.smartTree.ActionPresentation;
import com.intellij.ide.util.treeView.smartTree.ActionPresentationData;
import com.intellij.ide.util.treeView.smartTree.Filter;
import com.intellij.ide.util.treeView.smartTree.TreeElement;
import org.jastadd.tooling.aspect.psi.JastAddAspectAspectDeclaration;
import org.jastadd.tooling.aspect.psi.JastAddAspectAttribute;
import org.jastadd.tooling.util.JastAddIcons;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class JastAddAspectAttributeFilter implements Filter {
  @NonNls
  public static final String ID = "HIDE_ATTRIBUTE";

  @Override
  public boolean isVisible(TreeElement treeNode) {
    if (treeNode instanceof AspectStructureViewElement) {
      return !(((AspectStructureViewElement) treeNode).getValue() instanceof JastAddAspectAttribute);
    }
    else {
      return true;
    }
  }

  @Override
  @NotNull
  public ActionPresentation getPresentation() {
    // TODO use i18n and string bundle like JavaStructureViewBundle
    return new ActionPresentationData("Hide Attributes", null, JastAddIcons.ATTRIBUTE);
  }

  @Override
  @NotNull
  public String getName() {
    return ID;
  }

  @Override
  public boolean isReverted() {
    return false;
  }
}
