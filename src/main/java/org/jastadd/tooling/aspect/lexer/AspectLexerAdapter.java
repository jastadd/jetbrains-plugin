package org.jastadd.tooling.aspect.lexer;

import com.intellij.lexer.FlexAdapter;

public class AspectLexerAdapter extends FlexAdapter {

  public AspectLexerAdapter() {
    super(new AspectLexer(null));
  }

}
