package org.jastadd.tooling.aspect;

import com.intellij.lang.Language;
import com.intellij.lang.java.JavaLanguage;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.InjectedLanguagePlaces;
import com.intellij.psi.LanguageInjector;
import com.intellij.psi.PsiLanguageInjectionHost;
import org.jastadd.tooling.aspect.psi.JastAddAspectAspectClassDeclaration;
import org.jastadd.tooling.aspect.psi.JastAddAspectBlock;
import org.jastadd.tooling.aspect.psi.JastAddAspectExpression;
import org.jetbrains.annotations.NotNull;

public class JavaLanguageInjector implements LanguageInjector {

  /**
   * @param host                     PSI element inside which your language will be injected.
   * @param injectionPlacesRegistrar stores places where injection occurs. <br>
   *                                 Call its {@link InjectedLanguagePlaces#addPlace(Language, TextRange, String, String)}
   *                                 method to register particular injection place.
   *                                 For example, to inject your language in string literal inside quotes, you might want to <br>
   *                                 {@code injectionPlacesRegistrar.addPlace(myLanguage, new TextRange(1,host.getTextLength()-1))}
   */
  @Override
  public void getLanguagesToInject(@NotNull PsiLanguageInjectionHost host, @NotNull InjectedLanguagePlaces injectionPlacesRegistrar) {
    if (host.isValidHost()) {
      // TODO more injections
      if (host instanceof JastAddAspectBlock) {
        injectionPlacesRegistrar.addPlace(JavaLanguage.INSTANCE, new TextRange(1, host.getTextLength() - 1), "class Unused$ClassName { public void unused$MethodName() {", "}}");
      } else if (host instanceof JastAddAspectExpression) {
        injectionPlacesRegistrar.addPlace(JavaLanguage.INSTANCE, new TextRange(0, host.getTextLength()), "class Unused$ClassName { public void unused$MethodName() {Object unused$VariableName = ", ";}}");
      } else if (host instanceof JastAddAspectAspectClassDeclaration) {
        injectionPlacesRegistrar.addPlace(JavaLanguage.INSTANCE, new TextRange(0, host.getTextLength()), "", "");
      }
    }
  }
}
