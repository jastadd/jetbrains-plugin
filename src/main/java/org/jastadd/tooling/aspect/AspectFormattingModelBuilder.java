package org.jastadd.tooling.aspect;

import com.intellij.formatting.*;
import com.intellij.psi.codeStyle.CodeStyleSettings;
import org.jastadd.tooling.aspect.psi.AspectTypes;
import org.jetbrains.annotations.NotNull;

public class AspectFormattingModelBuilder implements FormattingModelBuilder {

  private static SpacingBuilder createSpaceBuilder(CodeStyleSettings settings) {
    return new SpacingBuilder(settings, Aspect.INSTANCE)
      .before(AspectTypes.ASPECT).none()
      .after(AspectTypes.ASPECT).spaces(1)
      .after(AspectTypes.LBRACE).none();
  }

  @Override
  public @NotNull FormattingModel createModel(@NotNull FormattingContext formattingContext) {
    final CodeStyleSettings codeStyleSettings = formattingContext.getCodeStyleSettings();
    return FormattingModelProvider
      .createFormattingModelForPsiFile(formattingContext.getContainingFile(),
        new AspectBlock(formattingContext.getNode(),
          Wrap.createWrap(WrapType.NONE, false),
          Alignment.createAlignment(),
          createSpaceBuilder(codeStyleSettings)),
        codeStyleSettings);
  }

}
