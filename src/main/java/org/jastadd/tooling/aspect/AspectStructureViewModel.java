package org.jastadd.tooling.aspect;


import com.intellij.ide.structureView.StructureViewModel;
import com.intellij.ide.structureView.StructureViewModelBase;
import com.intellij.ide.structureView.StructureViewTreeElement;
import com.intellij.ide.util.treeView.smartTree.Filter;
import com.intellij.ide.util.treeView.smartTree.Grouper;
import com.intellij.ide.util.treeView.smartTree.Sorter;
import com.intellij.psi.PsiFile;
import org.jastadd.tooling.aspect.psi.JastAddAspectAspectDeclaration;
import org.jastadd.tooling.aspect.psi.JastAddAspectAttribute;
import org.jetbrains.annotations.NotNull;

public class AspectStructureViewModel extends StructureViewModelBase implements
  StructureViewModel.ElementInfoProvider {

  public AspectStructureViewModel(PsiFile psiFile) {
    super(psiFile, new AspectStructureViewElement(psiFile));
  }

  @NotNull
  public Sorter @NotNull [] getSorters() {
    return new Sorter[]{Sorter.ALPHA_SORTER};
  }


  @Override
  public boolean isAlwaysShowsPlus(StructureViewTreeElement element) {
    return element.getValue() instanceof JastAddAspectAspectDeclaration;
  }

  @Override
  public boolean isAlwaysLeaf(StructureViewTreeElement element) {
    return element.getValue() instanceof JastAddAspectAttribute;
  }

  @Override
  public Grouper @NotNull [] getGroupers() {
    // TODO group by member type
    return new Grouper[]{};
  }

  @Override
  public Filter @NotNull [] getFilters() {
    return new Filter[]{new JastAddAspectAttributeFilter(), new JastAddAspectIntertypeFilter(), new JastAddAspectRewriteFilter()};
  }
}
