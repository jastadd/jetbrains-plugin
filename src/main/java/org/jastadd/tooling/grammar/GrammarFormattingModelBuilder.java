package org.jastadd.tooling.grammar;

import com.intellij.formatting.*;
import com.intellij.lang.ASTNode;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.PsiFile;
import com.intellij.psi.codeStyle.CodeStyleSettings;
import com.intellij.psi.codeStyle.CommonCodeStyleSettings;
import com.intellij.psi.tree.TokenSet;
import org.jastadd.tooling.grammar.parser.GrammarTypes;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class GrammarFormattingModelBuilder implements FormattingModelBuilder {

  private static SpacingBuilder createSpaceBuilder(CodeStyleSettings settings) {
    final CommonCodeStyleSettings commonSettings = settings.getCommonSettings(Grammar.INSTANCE.getID());

    final TokenSet roleMultiplicityTokens = TokenSet.create(GrammarTypes.STAR, GrammarTypes.QUESTION_MARK);
    final TokenSet relationDirectionTokens = TokenSet.create(GrammarTypes.LEFT, GrammarTypes.RIGHT, GrammarTypes.BIDIRECTIONAL);
    final TokenSet declarationTokens = TokenSet.create(GrammarTypes.TYPE_DECL, GrammarTypes.RELATION);

    return new SpacingBuilder(settings, Grammar.INSTANCE)
      .around(GrammarTypes.ASSIGN).spaceIf(commonSettings.SPACE_AROUND_ASSIGNMENT_OPERATORS)
      .before(declarationTokens).none()
      .before(GrammarTypes.SCOL).spaceIf(commonSettings.SPACE_BEFORE_SEMICOLON)
      .around(relationDirectionTokens).spaceIf(commonSettings.SPACE_AROUND_RELATIONAL_OPERATORS)
      .between(GrammarTypes.COMPONENT, GrammarTypes.COMPONENT).spaces(1)
      .around(GrammarTypes.DOT).none()
      .before(roleMultiplicityTokens).spaceIf(commonSettings.SPACE_AROUND_UNARY_OPERATOR)
      .aroundInside(GrammarTypes.COL, GrammarTypes.TYPE_DECL).spaceIf(commonSettings.SPACE_AROUND_ADDITIVE_OPERATORS)
      .aroundInside(GrammarTypes.COL, GrammarTypes.COMPONENT).none()
      .withinPair(GrammarTypes.LT, GrammarTypes.GT).spaceIf(commonSettings.SPACE_WITHIN_CAST_PARENTHESES)
      .withinPair(GrammarTypes.LBRACKET, GrammarTypes.RBRACKET).spaceIf(commonSettings.SPACE_WITHIN_BRACKETS)
      .between(declarationTokens, GrammarTypes.COMMENT).spaces(1)
      .afterInside(GrammarTypes.SLASH, GrammarTypes.NTA_COMPONENT).none()
      .beforeInside(GrammarTypes.SLASH, GrammarTypes.NTA_COMPONENT).none();
  }

  @Override
  public @NotNull FormattingModel createModel(FormattingContext formattingContext) {
    return FormattingModelProvider
      .createFormattingModelForPsiFile(formattingContext.getPsiElement().getContainingFile(),
        new GrammarBlock(formattingContext.getPsiElement().getNode(),
          Wrap.createWrap(WrapType.NONE, false),
          Alignment.createAlignment(),
          createSpaceBuilder(formattingContext.getCodeStyleSettings())),
        formattingContext.getCodeStyleSettings());
  }

  @Nullable
  @Override
  public TextRange getRangeAffectingIndent(PsiFile file, int offset, ASTNode elementAtOffset) {
    return null;
  }

}
