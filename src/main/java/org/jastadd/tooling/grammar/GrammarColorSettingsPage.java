package org.jastadd.tooling.grammar;


import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import org.jastadd.tooling.util.JastAddIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.Map;

@SuppressWarnings("DialogTitleCapitalization")
public class GrammarColorSettingsPage implements ColorSettingsPage {

  private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{

    new AttributesDescriptor("Keyword", GrammarSyntaxHighlighter.KEYWORD),
    new AttributesDescriptor("Production Rule Symbol", GrammarSyntaxHighlighter.ASSIGN),
    new AttributesDescriptor("Relation Direction", GrammarSyntaxHighlighter.RELATON_DIR),
    new AttributesDescriptor("Superclass/Component Name", GrammarSyntaxHighlighter.COL),
    new AttributesDescriptor("Comma", GrammarSyntaxHighlighter.COMMA),
    new AttributesDescriptor("Documentation Comment", GrammarSyntaxHighlighter.DOCCOMMENT),
    new AttributesDescriptor("Namespace Qualifier Dot", GrammarSyntaxHighlighter.DOT),
    new AttributesDescriptor("Identifier", GrammarSyntaxHighlighter.ID),
    new AttributesDescriptor("Terminal Symbol Bracket", GrammarSyntaxHighlighter.ANGLE_BRACKET),
    new AttributesDescriptor("Block Comment", GrammarSyntaxHighlighter.MULTILINECOMMENT),
    new AttributesDescriptor("0..1 Multiplicity", GrammarSyntaxHighlighter.QUESTION_MARK),
    new AttributesDescriptor("Optional Bracket", GrammarSyntaxHighlighter.BRACKET),
    new AttributesDescriptor("Semicolon", GrammarSyntaxHighlighter.SCOL),
    new AttributesDescriptor("Line Comment", GrammarSyntaxHighlighter.SINGLELINECOMMENT),
    new AttributesDescriptor("NTA Bracket", GrammarSyntaxHighlighter.SLASH),
    new AttributesDescriptor("Star Multiplicity", GrammarSyntaxHighlighter.STAR),
    new AttributesDescriptor("Bad Value", GrammarSyntaxHighlighter.BAD_CHARACTER)
  };

  @Nullable
  @Override
  public Icon getIcon() {
    return JastAddIcons.FILE;
  }

  @NotNull
  @Override
  public SyntaxHighlighter getHighlighter() {
    return new GrammarSyntaxHighlighter();
  }

  @NotNull
  @Override
  public String getDemoText() {
    return "Program ::= GrammarFile* <NameList:java.util.Map<String,String>>;\n" +
      "abstract Grammar ::= Declaration*; /* good rule! */\n" +
      "GrammarFile : Grammar ::= <FileName> ; // also good rule\n" +
      "/** the best rule */\n" +
      "abstract Declaration ::= Comment*  /Comment/;\n" +
      "TypeDecl:Declaration ::= <Name> <Abstract:boolean> Component* [Comment];\n" +
      "rel TypeDecl.SuperType? <-> TypeDecl.SubType*;\n" +
      "/* Bad character: */ #";
  }

  @Nullable
  @Override
  public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
    return null;
  }

  @NotNull
  @Override
  public AttributesDescriptor @NotNull [] getAttributeDescriptors() {
    return DESCRIPTORS;
  }

  @NotNull
  @Override
  public ColorDescriptor @NotNull [] getColorDescriptors() {
    return ColorDescriptor.EMPTY_ARRAY;
  }

  @NotNull
  @Override
  public String getDisplayName() {
    return "RelAst Grammar";
  }

}
