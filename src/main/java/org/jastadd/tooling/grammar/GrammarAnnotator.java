package org.jastadd.tooling.grammar;


import com.intellij.codeInspection.ProblemHighlightType;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.psi.PsiElement;
import org.jastadd.tooling.grammar.psi.GrammarComponent;
import org.jastadd.tooling.grammar.psi.GrammarTypeName;
import org.jetbrains.annotations.NotNull;

public class GrammarAnnotator implements Annotator {

  @Override
  public void annotate(@NotNull final PsiElement element, @NotNull AnnotationHolder holder) {

    if (element instanceof GrammarComponent) {
      GrammarComponent component = (GrammarComponent) element;
      if (component.getTypeName() != null && component.getComponentName() != null) {
        String name = component.getComponentName().getText();
        if (name != null && !name.equals("") && name.equals(component.getTypeName().getName())) {
          holder.newAnnotation(HighlightSeverity.WEAK_WARNING, "Redundant name")
            .range(component.getComponentName().getTextRange())
            .highlightType(ProblemHighlightType.WEAK_WARNING)
            .textAttributes(DefaultLanguageHighlighterColors.HIGHLIGHTED_REFERENCE)
            .tooltip("When the name of a component is the same as its type, it can be omitted.")
            .withFix(new GrammarRemoveRedundantComponentNameFix(component))
            .create();
        }
      }
    } else if (element instanceof GrammarTypeName) {
      GrammarTypeName reference = (GrammarTypeName) element;
      if (GrammarUtil.findTypeDecl(element.getProject(), reference.getName()).isEmpty()) {
        holder.newAnnotation(HighlightSeverity.ERROR, "Undefined reference")
          .range(element.getTextRange())
          .highlightType(ProblemHighlightType.ERROR)
          .tooltip("Reference to unknown nonterminal type.")
          .create();
      }
    }

  }

}
