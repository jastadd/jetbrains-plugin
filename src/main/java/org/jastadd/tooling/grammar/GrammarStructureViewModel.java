package org.jastadd.tooling.grammar;

import com.intellij.ide.structureView.StructureViewModel;
import com.intellij.ide.structureView.StructureViewModelBase;
import com.intellij.ide.structureView.StructureViewTreeElement;
import com.intellij.ide.util.treeView.smartTree.Sorter;
import com.intellij.psi.PsiFile;
import org.jastadd.tooling.grammar.psi.GrammarFile;
import org.jetbrains.annotations.NotNull;

public class GrammarStructureViewModel extends StructureViewModelBase implements
  StructureViewModel.ElementInfoProvider {

  public GrammarStructureViewModel(PsiFile psiFile) {
    super(psiFile, new GrammarStructureViewElement(psiFile));
  }

  @NotNull
  public Sorter @NotNull [] getSorters() {
    return new Sorter[]{Sorter.ALPHA_SORTER};
  }


  @Override
  public boolean isAlwaysShowsPlus(StructureViewTreeElement element) {
    return false;
  }

  @Override
  public boolean isAlwaysLeaf(StructureViewTreeElement element) {
    return element instanceof GrammarFile;
  }

}
