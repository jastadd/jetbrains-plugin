package org.jastadd.tooling.grammar;

import com.intellij.codeInsight.lookup.LookupElement;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.util.TextRange;
import com.intellij.psi.*;
import org.jastadd.tooling.grammar.psi.GrammarTypeDecl;
import org.jastadd.tooling.util.JastAddIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.List;

public class GrammarReference extends PsiReferenceBase<PsiElement> implements PsiPolyVariantReference {

  private final String key;

  public GrammarReference(@NotNull PsiElement element, TextRange textRange) {
    super(element, textRange);
    key = element.getText().substring(textRange.getStartOffset(), textRange.getEndOffset());
  }

  @NotNull
  @Override
  public ResolveResult @NotNull [] multiResolve(boolean incompleteCode) {
    Project project = myElement.getProject();
    final List<GrammarTypeDecl> typeDecls = GrammarUtil.findTypeDecl(project, key);
    List<ResolveResult> results = new ArrayList<>();
    for (GrammarTypeDecl typeDecl : typeDecls) {
      results.add(new PsiElementResolveResult(typeDecl));
    }
    return results.toArray(new ResolveResult[0]);
  }

  @Nullable
  @Override
  public PsiElement resolve() {
    ResolveResult[] resolveResults = multiResolve(false);
    return resolveResults.length == 1 ? resolveResults[0].getElement() : null;
  }

  @NotNull
  @Override
  public Object @NotNull [] getVariants() {
    Project project = myElement.getProject();
    List<GrammarTypeDecl> typeDecls = GrammarUtil.findTypeDecl(project);
    List<LookupElement> variants = new ArrayList<>();
    for (final GrammarTypeDecl typeDecl : typeDecls) {
      if (typeDecl.getName() != null && !typeDecl.getName().isEmpty()) {
        variants.add(LookupElementBuilder
          .create(typeDecl).withIcon(JastAddIcons.FILE)
          .withPresentableText(typeDecl.getName())
          .withTypeText(typeDecl.getContainingFile().getName())
        );
      }
    }
    return variants.toArray();
  }


}

