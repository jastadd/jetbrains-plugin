package org.jastadd.tooling.grammar;

import com.intellij.codeInsight.daemon.RelatedItemLineMarkerInfo;
import com.intellij.codeInsight.daemon.RelatedItemLineMarkerProvider;
import com.intellij.codeInsight.navigation.NavigationGutterIconBuilder;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiIdentifier;
import org.jastadd.tooling.grammar.psi.GrammarTypeDecl;
import org.jastadd.tooling.util.JastAddIcons;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import static org.jastadd.tooling.grammar.GrammarUtil.asReferenceToTypeDecl;

public class GrammarLineMarkerProvider extends RelatedItemLineMarkerProvider {

  @Override
  protected void collectNavigationMarkers(@NotNull PsiElement element,
                                          @NotNull Collection<? super RelatedItemLineMarkerInfo<?>> result) {

    if (!(element instanceof PsiIdentifier)) {
      return;
    }

    Optional<PsiClass> classOptional = asReferenceToTypeDecl(element.getParent());

    if (!classOptional.isPresent()) {
      return;
    }

    // Get the list of typeDecls for given key
    List<GrammarTypeDecl> typeDecls = GrammarUtil.findTypeDecl(element.getProject(), classOptional.get().getName());
    if (!typeDecls.isEmpty()) {
      NavigationGutterIconBuilder<PsiElement> builder =
        NavigationGutterIconBuilder.create(JastAddIcons.FILE)
          .setTargets(typeDecls)
          .setTooltipText("Navigate to production rule for RelAst nonterminal");
      result.add(builder.createLineMarkerInfo(element));
    }
  }

}
