package org.jastadd.tooling.grammar;

import com.intellij.lang.cacheBuilder.DefaultWordsScanner;
import com.intellij.lang.cacheBuilder.WordsScanner;
import com.intellij.lang.findUsages.FindUsagesProvider;
import com.intellij.psi.PsiElement;
import com.intellij.psi.tree.TokenSet;
import org.jastadd.tooling.grammar.lexer.GrammarLexerAdapter;
import org.jastadd.tooling.grammar.parser.GrammarTypes;
import org.jastadd.tooling.grammar.psi.GrammarTypeDecl;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class GrammarFindUsageProvider implements FindUsagesProvider {

  @Nullable
  @Override
  public WordsScanner getWordsScanner() {
    // TODO comments are not working
    return new DefaultWordsScanner(new GrammarLexerAdapter(),
      TokenSet.create(GrammarTypes.ID),
      TokenSet.create(GrammarTypes.MULTILINECOMMENT, GrammarTypes.DOCCOMMENT, GrammarTypes.SINGLELINECOMMENT),
      TokenSet.EMPTY);
  }

  @Override
  public boolean canFindUsagesFor(@NotNull PsiElement psiElement) {
    return psiElement instanceof GrammarTypeDecl; // was: PsiNamedElement
  }

  @Nullable
  @Override
  public String getHelpId(@NotNull PsiElement psiElement) {
    return null;
  }

  @NotNull
  @Override
  public String getType(@NotNull PsiElement element) {
    if (element instanceof GrammarTypeDecl) {
      return "JastAdd Nonterminal Type";
    } else {
      return "";
    }
  }

  @NotNull
  @Override
  public String getDescriptiveName(@NotNull PsiElement element) {
    if (element instanceof GrammarTypeDecl) {
      GrammarTypeDecl decl = (GrammarTypeDecl) element;
      if (decl.getName() != null) {
        return decl.getName();
      }
    }
    return "";
  }

  @NotNull
  @Override
  public String getNodeText(@NotNull PsiElement element, boolean useFullName) {
    if (element instanceof GrammarTypeDecl) {
      return element.getText();
    } else {
      return "";
    }
  }

}
