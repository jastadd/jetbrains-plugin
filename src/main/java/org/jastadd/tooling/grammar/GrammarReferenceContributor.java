package org.jastadd.tooling.grammar;

import com.intellij.openapi.util.TextRange;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.psi.*;
import com.intellij.util.ProcessingContext;
import org.jastadd.tooling.grammar.psi.GrammarTypeName;
import org.jetbrains.annotations.NotNull;

public class GrammarReferenceContributor extends PsiReferenceContributor {

  @Override
  public void registerReferenceProviders(@NotNull PsiReferenceRegistrar registrar) {
    registrar.registerReferenceProvider(PlatformPatterns.psiElement(GrammarTypeName.class),
      new PsiReferenceProvider() {
        @NotNull
        @Override
        public PsiReference @NotNull [] getReferencesByElement(@NotNull PsiElement element,
                                                               @NotNull ProcessingContext context) {

          GrammarTypeName typeReference = (GrammarTypeName) element;
          String value = typeReference.getText();
          if (value != null) {
            TextRange range = new TextRange(0, value.length());
            return new PsiReference[]{new GrammarReference(element, range)};
          }
          return PsiReference.EMPTY_ARRAY;
        }
      });
  }

}
