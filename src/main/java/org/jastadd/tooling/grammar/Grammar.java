package org.jastadd.tooling.grammar;

import com.intellij.lang.Language;

public class Grammar extends Language {

  public static final Grammar INSTANCE = new Grammar();

  private Grammar() {
    super("JastAddGrammar");
  }

}
