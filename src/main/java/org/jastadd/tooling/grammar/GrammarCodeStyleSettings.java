package org.jastadd.tooling.grammar;

import com.intellij.psi.codeStyle.CodeStyleSettings;
import com.intellij.psi.codeStyle.CustomCodeStyleSettings;

public class GrammarCodeStyleSettings extends CustomCodeStyleSettings {

  public GrammarCodeStyleSettings(CodeStyleSettings settings) {
    super("GrammarCodeStyleSettings", settings);
  }

}
