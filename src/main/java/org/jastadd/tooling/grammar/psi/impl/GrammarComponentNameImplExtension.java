package org.jastadd.tooling.grammar.psi.impl;

import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import org.jastadd.tooling.common.psi.impl.NamedElementImpl;
import org.jastadd.tooling.grammar.psi.GrammarComponentName;
import org.jastadd.tooling.grammar.psi.GrammarElementFactory;
import org.jastadd.tooling.common.psi.NamedElement;
import org.jetbrains.annotations.NotNull;

public class GrammarComponentNameImplExtension extends NamedElementImpl implements NamedElement {

  public GrammarComponentNameImplExtension(@NotNull ASTNode node) {
    super(node);
  }

  public String getName() {
    // this finds the *first* ID, which is what we want
    return getNode().getText();
  }

  public PsiElement setName(@NotNull String newName) {
    // FIXME this can break the grammar when the type is used in an unnamed component (and in many other cases probably)
    ASTNode keyNode = getNode().getFirstChildNode();
    if (keyNode != null) {
      GrammarComponentName name = GrammarElementFactory.createComponentName(getProject(), newName);
      ASTNode newKeyNode = name.getNode().getFirstChildNode();
      getNode().replaceChild(keyNode, newKeyNode);
    }
    return this;
  }

  public PsiElement getNameIdentifier() {
    return getNode().getPsi();
  }

}
