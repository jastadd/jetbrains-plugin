package org.jastadd.tooling.grammar.psi;

import com.intellij.openapi.util.TextRange;
import com.intellij.psi.AbstractElementManipulator;
import com.intellij.psi.ElementManipulator;
import com.intellij.util.IncorrectOperationException;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class GrammarTypeNameManipulator extends AbstractElementManipulator<GrammarTypeName> implements ElementManipulator<GrammarTypeName> {
  /**
   * Changes the element's text to the given new text.
   *
   * @param element    element to be changed
   * @param range      range within the element
   * @param newContent new element text
   * @return changed element
   * @throws IncorrectOperationException if something goes wrong
   */
  @Nullable
  @Override
  public GrammarTypeName handleContentChange(@NotNull GrammarTypeName element, @NotNull TextRange range, String newContent) {
    try {
      return (GrammarTypeName) element.setName(range.replace(element.getText(), newContent));
    } catch (Exception e) { // e.g., in case the range is wrong
      throw new IncorrectOperationException(e);
    }
  }
}
