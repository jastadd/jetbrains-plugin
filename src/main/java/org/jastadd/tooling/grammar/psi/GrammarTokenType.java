package org.jastadd.tooling.grammar.psi;

import com.intellij.psi.tree.IElementType;
import org.jastadd.tooling.grammar.Grammar;
import org.jetbrains.annotations.NonNls;
import org.jetbrains.annotations.NotNull;

public class GrammarTokenType extends IElementType {

  public GrammarTokenType(@NotNull @NonNls String debugName) {
    super(debugName, Grammar.INSTANCE);
  }

  @Override
  public String toString() {
    return "GrammarTokenType." + super.toString();
  }

}
