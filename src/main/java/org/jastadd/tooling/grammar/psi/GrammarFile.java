package org.jastadd.tooling.grammar.psi;

import com.intellij.extapi.psi.PsiFileBase;
import com.intellij.openapi.fileTypes.FileType;
import com.intellij.psi.FileViewProvider;
import org.jastadd.tooling.grammar.Grammar;
import org.jastadd.tooling.grammar.GrammarFileType;
import org.jetbrains.annotations.NotNull;

public class GrammarFile extends PsiFileBase {

  public GrammarFile(@NotNull FileViewProvider viewProvider) {
    super(viewProvider, Grammar.INSTANCE);
  }

  @NotNull
  @Override
  public FileType getFileType() {
    return GrammarFileType.INSTANCE;
  }

  @Override
  public String toString() {
    return "RelAst Grammar File";
  }

}
