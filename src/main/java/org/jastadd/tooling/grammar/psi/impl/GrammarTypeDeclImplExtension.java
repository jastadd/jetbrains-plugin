package org.jastadd.tooling.grammar.psi.impl;

import com.intellij.lang.ASTNode;
import com.intellij.psi.PsiElement;
import org.jastadd.tooling.common.psi.impl.NamedElementImpl;
import org.jastadd.tooling.grammar.parser.GrammarTypes;
import org.jastadd.tooling.grammar.psi.GrammarElementFactory;
import org.jastadd.tooling.common.psi.NamedElement;
import org.jastadd.tooling.grammar.psi.GrammarTypeName;
import org.jetbrains.annotations.NotNull;

public class GrammarTypeDeclImplExtension extends NamedElementImpl implements NamedElement {

  public GrammarTypeDeclImplExtension(@NotNull ASTNode node) {
    super(node);
  }

  public String getName() {
    // this finds the *first* ID, which is what we want
    ASTNode nameNode = getNode().findChildByType(GrammarTypes.TYPE_NAME);
    if (nameNode != null) {
      return nameNode.getText();
    } else {
      return null;
    }
  }

  public PsiElement setName(@NotNull String newName) {
    ASTNode nameNode = getNode().findChildByType(GrammarTypes.TYPE_NAME);
    if (nameNode != null) {
      GrammarTypeName name = GrammarElementFactory.createTypeName(getProject(), newName);
      ASTNode newKeyNode = name.getNode();
      getNode().replaceChild(nameNode, newKeyNode);
    }
    return this;
  }

  public PsiElement getNameIdentifier() {
    ASTNode nameNode = getNode().findChildByType(GrammarTypes.TYPE_NAME);
    if (nameNode != null) {
      return nameNode.getPsi();
    } else {
      return null;
    }
  }


}
