package org.jastadd.tooling.grammar.psi;

import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiFileFactory;
import org.jastadd.tooling.grammar.GrammarFileType;
import org.jastadd.tooling.grammar.parser.GrammarTypes;

import java.util.Objects;

public class GrammarElementFactory {

  private GrammarElementFactory() {
    throw new IllegalStateException("Utility class");
  }

  public static GrammarTypeName createTypeName(Project project, String name) {
    final GrammarFile file = createFile(project, name + ";");
    return (GrammarTypeName) (Objects.requireNonNull(file.getFirstChild().getNode().findChildByType(GrammarTypes.TYPE_NAME)).getPsi());
  }

  public static GrammarComponentName createComponentName(Project project, String name) {
    final GrammarFile file = createFile(project, "X ::= " + name + ":X ;");
    return (GrammarComponentName) file.getFirstChild().getFirstChild();
  }

  public static GrammarFile createFile(Project project, String text) {
    String name = "dummy.relast";
    return (GrammarFile) PsiFileFactory.getInstance(project).
      createFileFromText(name, GrammarFileType.INSTANCE, text);
  }
}
