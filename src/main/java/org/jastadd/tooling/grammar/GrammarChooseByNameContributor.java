package org.jastadd.tooling.grammar;

import com.intellij.navigation.ChooseByNameContributor;
import com.intellij.navigation.NavigationItem;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import org.jastadd.tooling.grammar.psi.GrammarTypeDecl;
import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

public class GrammarChooseByNameContributor implements ChooseByNameContributor {

  @NotNull
  @Override
  public String @NotNull [] getNames(Project project, boolean includeNonProjectItems) {
    List<GrammarTypeDecl> typeDecls = GrammarUtil.findTypeDecl(project);
    List<String> names = new ArrayList<>(typeDecls.size());
    for (GrammarTypeDecl typeDecl : typeDecls) {
      if (typeDecl.getName() != null && !typeDecl.getName().isEmpty()) {
        names.add(typeDecl.getName());
      }
    }
    return names.toArray(new String[0]);
  }

  @NotNull
  @Override
  public NavigationItem @NotNull [] getItemsByName(String name, String pattern, Project project, boolean includeNonProjectItems) {
    // TODO: include non project items

    List<NavigationItem> items = new ArrayList<>();
    for (PsiElement typeDecl : GrammarUtil.findTypeDecl(project, name)) {
      items.add((NavigationItem) typeDecl);
    }

    return items.toArray(new NavigationItem[0]);
  }

}
