package org.jastadd.tooling.grammar;


import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.HighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.psi.TokenType;
import com.intellij.psi.tree.IElementType;
import org.jastadd.tooling.grammar.lexer.GrammarLexerAdapter;
import org.jastadd.tooling.grammar.parser.GrammarTypes;
import org.jetbrains.annotations.NotNull;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

public class GrammarSyntaxHighlighter extends SyntaxHighlighterBase {

  public static final TextAttributesKey KEYWORD = createTextAttributesKey("RELAST_KEYWORD", DefaultLanguageHighlighterColors.KEYWORD);
  public static final TextAttributesKey ASSIGN = createTextAttributesKey("RELAST_ASSIGN", DefaultLanguageHighlighterColors.OPERATION_SIGN);
  public static final TextAttributesKey RELATON_DIR = createTextAttributesKey("RELAST_RELATION_DIR", DefaultLanguageHighlighterColors.OPERATION_SIGN);
  public static final TextAttributesKey COL = createTextAttributesKey("RELAST_COL", DefaultLanguageHighlighterColors.DOT);
  public static final TextAttributesKey COMMA = createTextAttributesKey("RELAST_COMMA", DefaultLanguageHighlighterColors.COMMA);
  public static final TextAttributesKey DOCCOMMENT = createTextAttributesKey("RELAST_DOCCOMMENT", DefaultLanguageHighlighterColors.DOC_COMMENT);
  public static final TextAttributesKey DOT = createTextAttributesKey("RELAST_DOT", DefaultLanguageHighlighterColors.DOT);
  public static final TextAttributesKey ID = createTextAttributesKey("RELAST_ID", DefaultLanguageHighlighterColors.IDENTIFIER);
  public static final TextAttributesKey BRACKET = createTextAttributesKey("RELAST_BRACKET", DefaultLanguageHighlighterColors.BRACKETS);
  public static final TextAttributesKey ANGLE_BRACKET = createTextAttributesKey("ANGLE_BRACKET", DefaultLanguageHighlighterColors.BRACKETS);
  public static final TextAttributesKey MULTILINECOMMENT = createTextAttributesKey("RELAST_MULTILINECOMMENT", DefaultLanguageHighlighterColors.BLOCK_COMMENT);
  public static final TextAttributesKey QUESTION_MARK = createTextAttributesKey("RELAST_QUESTION_MARK", DefaultLanguageHighlighterColors.OPERATION_SIGN);
  public static final TextAttributesKey SCOL = createTextAttributesKey("RELAST_SCOL", DefaultLanguageHighlighterColors.SEMICOLON);
  public static final TextAttributesKey SINGLELINECOMMENT = createTextAttributesKey("RELAST_SINGLELINECOMMENT", DefaultLanguageHighlighterColors.LINE_COMMENT);
  public static final TextAttributesKey SLASH = createTextAttributesKey("RELAST_SLASH", DefaultLanguageHighlighterColors.BRACKETS);
  public static final TextAttributesKey STAR = createTextAttributesKey("RELAST_STAR", DefaultLanguageHighlighterColors.OPERATION_SIGN);
  public static final TextAttributesKey BAD_CHARACTER = createTextAttributesKey("SIMPLE_BAD_CHARACTER", HighlighterColors.BAD_CHARACTER);

  private static final TextAttributesKey[] KEYWORD_KEYS = new TextAttributesKey[]{KEYWORD};
  private static final TextAttributesKey[] ASSIGN_KEYS = new TextAttributesKey[]{ASSIGN};
  private static final TextAttributesKey[] RELATION_DIR_KEYS = new TextAttributesKey[]{RELATON_DIR};
  private static final TextAttributesKey[] COL_KEYS = new TextAttributesKey[]{COL};
  private static final TextAttributesKey[] COMMA_KEYS = new TextAttributesKey[]{COMMA};
  private static final TextAttributesKey[] DOCCOMMENT_KEYS = new TextAttributesKey[]{DOCCOMMENT};
  private static final TextAttributesKey[] DOT_KEYS = new TextAttributesKey[]{DOT};
  private static final TextAttributesKey[] ID_KEYS = new TextAttributesKey[]{ID};
  private static final TextAttributesKey[] ANGLE_BRACKET_KEYS = new TextAttributesKey[]{ANGLE_BRACKET};
  private static final TextAttributesKey[] MULTILINECOMMENT_KEYS = new TextAttributesKey[]{MULTILINECOMMENT};
  private static final TextAttributesKey[] QUESTION_MARK_KEYS = new TextAttributesKey[]{QUESTION_MARK};
  private static final TextAttributesKey[] BRACKET_KEYS = new TextAttributesKey[]{BRACKET};
  private static final TextAttributesKey[] SCOL_KEYS = new TextAttributesKey[]{SCOL};
  private static final TextAttributesKey[] SINGLELINECOMMENT_KEYS = new TextAttributesKey[]{SINGLELINECOMMENT};
  private static final TextAttributesKey[] SLASH_KEYS = new TextAttributesKey[]{SLASH};
  private static final TextAttributesKey[] STAR_KEYS = new TextAttributesKey[]{STAR};

  private static final TextAttributesKey[] BAD_CHAR_KEYS = new TextAttributesKey[]{BAD_CHARACTER};
  private static final TextAttributesKey[] EMPTY_KEYS = new TextAttributesKey[0];

  @NotNull
  @Override
  public Lexer getHighlightingLexer() {
    return new GrammarLexerAdapter();
  }

  @NotNull
  @Override
  public TextAttributesKey @NotNull [] getTokenHighlights(IElementType tokenType) {

    if (tokenType.equals(GrammarTypes.ABSTRACT)) {
      return KEYWORD_KEYS;
    } else if (tokenType.equals(GrammarTypes.ASSIGN)) {
      return ASSIGN_KEYS;
    } else if (tokenType.equals(GrammarTypes.BIDIRECTIONAL)) {
      return RELATION_DIR_KEYS;
    } else if (tokenType.equals(GrammarTypes.COL)) {
      return COL_KEYS;
    } else if (tokenType.equals(GrammarTypes.COMMA)) {
      return COMMA_KEYS;
    } else if (tokenType.equals(GrammarTypes.DOCCOMMENT)) {
      return DOCCOMMENT_KEYS;
    } else if (tokenType.equals(GrammarTypes.DOT)) {
      return DOT_KEYS;
    } else if (tokenType.equals(GrammarTypes.GT)) {
      return ANGLE_BRACKET_KEYS;
    } else if (tokenType.equals(GrammarTypes.ID)) {
      return ID_KEYS;
    } else if (tokenType.equals(GrammarTypes.LBRACKET)) {
      return BRACKET_KEYS;
    } else if (tokenType.equals(GrammarTypes.LEFT)) {
      return RELATION_DIR_KEYS;
    } else if (tokenType.equals(GrammarTypes.LT)) {
      return ANGLE_BRACKET_KEYS;
    } else if (tokenType.equals(GrammarTypes.MULTILINECOMMENT)) {
      return MULTILINECOMMENT_KEYS;
    } else if (tokenType.equals(GrammarTypes.QUESTION_MARK)) {
      return QUESTION_MARK_KEYS;
    } else if (tokenType.equals(GrammarTypes.RBRACKET)) {
      return BRACKET_KEYS;
    } else if (tokenType.equals(GrammarTypes.REL)) {
      return KEYWORD_KEYS;
    } else if (tokenType.equals(GrammarTypes.RIGHT)) {
      return RELATION_DIR_KEYS;
    } else if (tokenType.equals(GrammarTypes.SCOL)) {
      return SCOL_KEYS;
    } else if (tokenType.equals(GrammarTypes.SINGLELINECOMMENT)) {
      return SINGLELINECOMMENT_KEYS;
    } else if (tokenType.equals(GrammarTypes.SLASH)) {
      return SLASH_KEYS;
    } else if (tokenType.equals(GrammarTypes.STAR)) {
      return STAR_KEYS;
    } else if (tokenType.equals(TokenType.BAD_CHARACTER)) {
      return BAD_CHAR_KEYS;
    } else {
      return EMPTY_KEYS;
    }

  }

}
