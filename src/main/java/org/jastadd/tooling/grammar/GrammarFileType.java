package org.jastadd.tooling.grammar;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jastadd.tooling.util.JastAddIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class GrammarFileType extends LanguageFileType {

  public static final GrammarFileType INSTANCE = new GrammarFileType();

  private GrammarFileType() {
    super(Grammar.INSTANCE);
  }

  @NotNull
  @Override
  public String getName() {
    return "JastAdd Grammar";
  }

  @NotNull
  @Override
  public String getDescription() {
    return "JastAdd RAG Grammar";
  }

  @NotNull
  @Override
  public String getDefaultExtension() {
    return "ast";
  }

  @Nullable
  @Override
  public Icon getIcon() {
    return JastAddIcons.FILE;
  }

}
