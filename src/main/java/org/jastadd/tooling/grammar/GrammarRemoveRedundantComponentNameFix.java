package org.jastadd.tooling.grammar;

import com.intellij.codeInsight.intention.impl.BaseIntentionAction;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.project.Project;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiFile;
import com.intellij.util.IncorrectOperationException;
import org.jastadd.tooling.grammar.parser.GrammarTypes;
import org.jastadd.tooling.grammar.psi.GrammarComponent;
import org.jetbrains.annotations.NotNull;

public class GrammarRemoveRedundantComponentNameFix extends BaseIntentionAction {

  private final GrammarComponent component;

  public GrammarRemoveRedundantComponentNameFix(GrammarComponent component) {
    this.component = component;
  }

  @NotNull
  @Override
  public String getText() {
    return "Remove the name of component '" + component.getText() + "'";
  }

  @NotNull
  @Override
  public String getFamilyName() {
    return "Edit component";
  }

  @Override
  public boolean isAvailable(@NotNull Project project, Editor editor, PsiFile file) {
    return true;
  }

  @Override
  public void invoke(@NotNull final Project project, final Editor editor, PsiFile file) throws
    IncorrectOperationException {
    PsiElement first = component.getComponentName();
    PsiElement last = component.getComponentName();
    while (last != null && last.getNode().getElementType() != GrammarTypes.COL) {
      last = last.getNextSibling();
    }
    if (last == null) {
      throw new IncorrectOperationException("Unable to find ':' in component definition.");
    } else {
      component.deleteChildRange(first, last);
    }
  }


}
