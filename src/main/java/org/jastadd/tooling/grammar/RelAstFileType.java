package org.jastadd.tooling.grammar;

import com.intellij.openapi.fileTypes.LanguageFileType;
import org.jastadd.tooling.util.JastAddIcons;
import org.jetbrains.annotations.Nls;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;

public class RelAstFileType extends LanguageFileType {

  public static final RelAstFileType INSTANCE = new RelAstFileType();

  private RelAstFileType() {
    super(Grammar.INSTANCE);
  }

  @NotNull
  @Override
  public String getName() {
    return "RelAst Grammar";
  }

  @Override
  public @Nls @NotNull String getDisplayName() {
    return getName();
  }

  @NotNull
  @Override
  public String getDescription() {
    return "Relational RAG Grammar";
  }

  @NotNull
  @Override
  public String getDefaultExtension() {
    return "relast";
  }

  @Nullable
  @Override
  public Icon getIcon() {
    return JastAddIcons.FILE;
  }

}
