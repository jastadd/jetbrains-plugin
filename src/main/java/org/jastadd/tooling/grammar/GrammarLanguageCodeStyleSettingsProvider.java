package org.jastadd.tooling.grammar;

import com.intellij.lang.Language;
import com.intellij.psi.codeStyle.CodeStyleSettingsCustomizable;
import com.intellij.psi.codeStyle.LanguageCodeStyleSettingsProvider;
import org.jetbrains.annotations.NotNull;

public class GrammarLanguageCodeStyleSettingsProvider extends LanguageCodeStyleSettingsProvider {

  @NotNull
  @Override
  public Language getLanguage() {
    return Grammar.INSTANCE;
  }

  @Override
  public void customizeSettings(@NotNull CodeStyleSettingsCustomizable consumer, @NotNull SettingsType settingsType) {
    if (settingsType == SettingsType.SPACING_SETTINGS) {
      consumer.showStandardOptions("SPACE_AROUND_ASSIGNMENT_OPERATORS");
      consumer.renameStandardOption("SPACE_AROUND_ASSIGNMENT_OPERATORS", "Production Symbol (::=)");

      consumer.showStandardOptions("SPACE_BEFORE_SEMICOLON");
      consumer.renameStandardOption("SPACE_BEFORE_SEMICOLON", "Before semicolon");

      consumer.showStandardOptions("SPACE_AROUND_RELATIONAL_OPERATORS");
      consumer.renameStandardOption("SPACE_AROUND_RELATIONAL_OPERATORS", "Relation Directions (->,<->,<-)");

      consumer.showStandardOptions("SPACE_AROUND_UNARY_OPERATOR");
      consumer.renameStandardOption("SPACE_AROUND_UNARY_OPERATOR", "Multiplicity Operators (*,?)");

      consumer.showStandardOptions("SPACE_AROUND_ADDITIVE_OPERATORS");
      consumer.renameStandardOption("SPACE_AROUND_ADDITIVE_OPERATORS", "Superclass separator (:)");

      consumer.showStandardOptions("SPACE_WITHIN_CAST_PARENTHESES");
      consumer.renameStandardOption("SPACE_WITHIN_CAST_PARENTHESES", "Tokens");

      consumer.showStandardOptions("SPACE_WITHIN_BRACKETS");
      consumer.renameStandardOption("SPACE_WITHIN_BRACKETS", "Optional components");
    } else if (settingsType == SettingsType.BLANK_LINES_SETTINGS) {
      consumer.showStandardOptions("KEEP_BLANK_LINES_IN_CODE");
    }
  }

  @Override
  public String getCodeSample(@NotNull SettingsType settingsType) {
    return "Program ::= GrammarFile* <NameList:java.util.Map<String,String>>;\n" +
      "abstract Grammar ::= Declaration*; /* good rule! */\n" +
      "GrammarFile : Grammar ::= <FileName> ; // also good rule\n" +
      "/** the best rule */\n" +
      "abstract Declaration ::= Comment*  /Comment/;\n" +
      "TypeDecl:Declaration ::= <Name> <Abstract:boolean> Component* [Comment];\n\n\n" +
      "rel TypeDecl.SuperType? <-> TypeDecl.SubType*;\n";
  }

}
