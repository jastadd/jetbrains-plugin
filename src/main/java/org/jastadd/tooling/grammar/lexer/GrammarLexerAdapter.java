package org.jastadd.tooling.grammar.lexer;

import com.intellij.lexer.FlexAdapter;

public class GrammarLexerAdapter extends FlexAdapter {

  public GrammarLexerAdapter() {
    super(new GrammarLexer(null));
  }

}
