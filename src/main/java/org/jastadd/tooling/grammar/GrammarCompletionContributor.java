package org.jastadd.tooling.grammar;

import com.intellij.codeInsight.completion.*;
import com.intellij.codeInsight.lookup.LookupElementBuilder;
import com.intellij.patterns.PlatformPatterns;
import com.intellij.util.ProcessingContext;
import org.jastadd.tooling.grammar.parser.GrammarTypes;
import org.jastadd.tooling.grammar.psi.GrammarElementFactory;
import org.jastadd.tooling.grammar.psi.GrammarTypeDecl;
import org.jastadd.tooling.grammar.psi.GrammarTypeName;
import org.jastadd.tooling.util.JastAddIcons;
import org.jetbrains.annotations.NotNull;

public class GrammarCompletionContributor extends CompletionContributor {

  public static final String COMPLETION_ELEMENT_SUFFIX = "IntellijIdeaRulezzz";

  public GrammarCompletionContributor() {

    // add completion in definition of relations
    extend(CompletionType.BASIC, PlatformPatterns.psiElement(GrammarTypes.ID).afterLeaf("rel", "<->", "->", "<-"),
      new CompletionProvider<CompletionParameters>() {
        public void addCompletions(@NotNull CompletionParameters parameters,
                                   @NotNull ProcessingContext context,
                                   @NotNull CompletionResultSet resultSet) {
          for (GrammarTypeDecl decl : GrammarUtil.findTypeDecl(parameters.getPosition().getProject())) {
            GrammarTypeName ref = GrammarElementFactory.createTypeName(parameters.getPosition().getProject(), decl.getName());
            resultSet.addElement(LookupElementBuilder.create(ref).withIcon(JastAddIcons.FILE).withTypeText(decl.getContainingFile().getName()));
          }
        }
      }
    );

    // add "* ", "? " after REL ID DOT ID
    extend(CompletionType.BASIC, PlatformPatterns.psiElement(GrammarTypes.ID)
        .withTextLengthLongerThan(COMPLETION_ELEMENT_SUFFIX.length()) // see https://intellij-support.jetbrains.com/hc/en-us/community/posts/206752355-The-dreaded-IntellijIdeaRulezzz-string
        .afterLeaf(".")
        .afterLeaf(PlatformPatterns.psiElement().afterLeaf(PlatformPatterns.psiElement(GrammarTypes.ID)))
        .afterLeaf(PlatformPatterns.psiElement().afterLeaf(PlatformPatterns.psiElement().afterLeaf("rel"))),
      new CompletionProvider<CompletionParameters>() {
        public void addCompletions(@NotNull CompletionParameters parameters,
                                   @NotNull ProcessingContext context,
                                   @NotNull CompletionResultSet resultSet) {
          resultSet = resultSet.withPrefixMatcher("");
          resultSet.addElement(LookupElementBuilder.create("* ").bold().withPresentableText("*"));
          resultSet.addElement(LookupElementBuilder.create("? ").bold().withPresentableText("?"));
        }
      }
    );
    // add "*", "?" after <-  ID DOT ID
    // add "*", "?" after <-> ID DOT ID
    extend(CompletionType.BASIC, PlatformPatterns.psiElement(GrammarTypes.ID)
        .withTextLengthLongerThan(COMPLETION_ELEMENT_SUFFIX.length()) // see https://intellij-support.jetbrains.com/hc/en-us/community/posts/206752355-The-dreaded-IntellijIdeaRulezzz-string
        .afterLeaf(".")
        .afterLeaf(PlatformPatterns.psiElement().afterLeaf(PlatformPatterns.psiElement(GrammarTypes.ID)))
        .afterLeaf(PlatformPatterns.psiElement().afterLeaf(PlatformPatterns.psiElement().afterLeaf("<-", "<->"))),
      new CompletionProvider<CompletionParameters>() {
        public void addCompletions(@NotNull CompletionParameters parameters,
                                   @NotNull ProcessingContext context,
                                   @NotNull CompletionResultSet resultSet) {
          resultSet = resultSet.withPrefixMatcher("");
          resultSet.addElement(LookupElementBuilder.create("*").bold());
          resultSet.addElement(LookupElementBuilder.create("?").bold());
        }
      }
    );
    // add " -> ", " <-> " after REL ID DOT ID <SPACE>
    extend(CompletionType.BASIC, PlatformPatterns.psiElement(GrammarTypes.ID)
        .afterLeaf(PlatformPatterns.psiElement(GrammarTypes.ID))
        .afterLeaf(PlatformPatterns.psiElement().afterLeaf("."))
        .afterLeaf(PlatformPatterns.psiElement().afterLeaf(PlatformPatterns.psiElement().afterLeaf(PlatformPatterns.psiElement(GrammarTypes.ID))))
        .afterLeaf(PlatformPatterns.psiElement().afterLeaf(PlatformPatterns.psiElement().afterLeaf(PlatformPatterns.psiElement().afterLeaf("rel")))),
      new CompletionProvider<CompletionParameters>() {
        public void addCompletions(@NotNull CompletionParameters parameters,
                                   @NotNull ProcessingContext context,
                                   @NotNull CompletionResultSet resultSet) {
          resultSet.addElement(LookupElementBuilder.create("-> ").bold().withPresentableText("->"));
          resultSet.addElement(LookupElementBuilder.create("<-> ").bold().withPresentableText("<->"));
        }
      }
    );
    // add " -> ", " <-> " after REL ID DOT ID * <SPACE>
    // add " -> ", " <-> " after REL ID DOT ID ? <SPACE>
    extend(CompletionType.BASIC, PlatformPatterns.psiElement(GrammarTypes.ID)
        .afterLeaf("?", "*")
        .afterLeaf(PlatformPatterns.psiElement().afterLeaf(PlatformPatterns.psiElement(GrammarTypes.ID)))
        .afterLeaf(PlatformPatterns.psiElement().afterLeaf(PlatformPatterns.psiElement().afterLeaf(".")))
        .afterLeaf(PlatformPatterns.psiElement().afterLeaf(PlatformPatterns.psiElement().afterLeaf(PlatformPatterns.psiElement().afterLeaf(PlatformPatterns.psiElement(GrammarTypes.ID)))))
        .afterLeaf(PlatformPatterns.psiElement().afterLeaf(PlatformPatterns.psiElement().afterLeaf(PlatformPatterns.psiElement().afterLeaf(PlatformPatterns.psiElement().afterLeaf("rel"))))),
      new CompletionProvider<CompletionParameters>() {
        public void addCompletions(@NotNull CompletionParameters parameters,
                                   @NotNull ProcessingContext context,
                                   @NotNull CompletionResultSet resultSet) {
          resultSet.addElement(LookupElementBuilder.create("-> ").bold().withPresentableText("->"));
          resultSet.addElement(LookupElementBuilder.create("<-> ").bold().withPresentableText("<->"));
        }
      }
    );
    // add "." after REL ID
    // add "." after <-> ID
    // add "." after <- ID
    extend(CompletionType.BASIC, PlatformPatterns.psiElement(GrammarTypes.ID)
        .withTextLengthLongerThan(COMPLETION_ELEMENT_SUFFIX.length()) // see https://intellij-support.jetbrains.com/hc/en-us/community/posts/206752355-The-dreaded-IntellijIdeaRulezzz-string
        .afterLeaf("rel", "<->", "<-"),
      new CompletionProvider<CompletionParameters>() {
        public void addCompletions(@NotNull CompletionParameters parameters,
                                   @NotNull ProcessingContext context,
                                   @NotNull CompletionResultSet resultSet) {
          resultSet = resultSet.withPrefixMatcher("");
          resultSet.addElement(LookupElementBuilder.create(".").bold());
        }
      }
    );
    // add " <- " after REL ID <SPACE>
    extend(CompletionType.BASIC, PlatformPatterns.psiElement(GrammarTypes.ID)
        .afterLeaf(PlatformPatterns.psiElement(GrammarTypes.ID))
        .afterLeaf(PlatformPatterns.psiElement().afterLeaf("rel")),
      new CompletionProvider<CompletionParameters>() {
        public void addCompletions(@NotNull CompletionParameters parameters,
                                   @NotNull ProcessingContext context,
                                   @NotNull CompletionResultSet resultSet) {
          resultSet.addElement(LookupElementBuilder.create("<- ").bold().withPresentableText("<-"));
        }
      }
    );

  }

}
