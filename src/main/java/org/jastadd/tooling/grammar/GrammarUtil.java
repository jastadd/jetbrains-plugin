package org.jastadd.tooling.grammar;


import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;
import com.intellij.psi.javadoc.PsiDocComment;
import com.intellij.psi.javadoc.PsiDocTag;
import com.intellij.psi.search.FileTypeIndex;
import com.intellij.psi.search.GlobalSearchScope;
import com.intellij.psi.util.PsiTreeUtil;
import org.jastadd.tooling.grammar.psi.GrammarFile;
import org.jastadd.tooling.grammar.psi.GrammarTypeDecl;
import org.jetbrains.annotations.NotNull;

import java.util.*;


public class GrammarUtil {

  private GrammarUtil() {
    // appease SonarLint java:S1118
    throw new IllegalStateException("Utility class");
  }

  /**
   * Searches the entire project for RelAst grammar files with TypeDecls with the given name.
   *
   * @param project current project
   * @param name    to check
   * @return matching TypeDecls
   */
  public static List<GrammarTypeDecl> findTypeDecl(Project project, String name) {
    List<GrammarTypeDecl> result = new ArrayList<>();
    Collection<VirtualFile> virtualFiles =
      FileTypeIndex.getFiles(GrammarFileType.INSTANCE, GlobalSearchScope.allScope(project));
    for (VirtualFile virtualFile : virtualFiles) {
      GrammarFile simpleFile = (GrammarFile) PsiManager.getInstance(project).findFile(virtualFile);
      if (simpleFile != null) {
        // TODO check if file is ignored or generated!
        GrammarTypeDecl[] typeDecls = PsiTreeUtil.getChildrenOfType(simpleFile, GrammarTypeDecl.class);
        if (typeDecls != null) {
          for (GrammarTypeDecl typeDecl : typeDecls) {
            if (name.equals(typeDecl.getName())) {
              result.add(typeDecl);
            }
          }
        }
      }
    }
    return result;
  }

  /**
   * Collects all TypeDecls from all RelAst grammar files in the entire project.
   *
   * @param project current project
   * @return all TypeDecls
   */
  public static List<GrammarTypeDecl> findTypeDecl(Project project) {
    List<GrammarTypeDecl> result = new ArrayList<>();
    Collection<VirtualFile> virtualFiles =
      FileTypeIndex.getFiles(GrammarFileType.INSTANCE, GlobalSearchScope.allScope(project));
    for (VirtualFile virtualFile : virtualFiles) {
      GrammarFile simpleFile = (GrammarFile) PsiManager.getInstance(project).findFile(virtualFile);
      if (simpleFile != null) {
        // TODO check if file is ignored or generated!
        GrammarTypeDecl[] typeDecls = PsiTreeUtil.getChildrenOfType(simpleFile, GrammarTypeDecl.class);
        if (typeDecls != null) {
          result.addAll(Arrays.asList(typeDecls));
        }
      }
    }
    return result;
  }

  public static Optional<PsiClass> asReferenceToTypeDecl(@NotNull final PsiElement element) {

    // Ensure the Psi Element is a reference
    if (!(element instanceof PsiJavaCodeReferenceElement)) {
      return Optional.empty();
    }

    // Ensure the Psi element references something with a name
    PsiJavaCodeReferenceElement javaCodeReferenceElement = (PsiJavaCodeReferenceElement) element;
    String value = javaCodeReferenceElement.getReferenceName();
    if (value == null) {
      return Optional.empty();
    }

    // Ensure that the reference is valid and accessible
    JavaResolveResult result = javaCodeReferenceElement.advancedResolve(true);
    if (!result.isValidResult() || !result.isAccessible() || result.getElement() == null) {
      return Optional.empty();
    }

    // Ensure that the reference is a Java class
    PsiElement reference = result.getElement();
    if (!(reference instanceof PsiClass)) {
      return Optional.empty();
    }

    // Ensure that the class has a doc comment which contains the tag ast with the value node
    PsiClass refClass = (PsiClass) reference;
    PsiDocComment docComment = refClass.getDocComment();
    if (docComment == null) {
      return Optional.empty();
    }
    PsiDocTag astTag = docComment.findTagByName("ast");
    if (astTag == null) {
      return Optional.empty();
    }
    if (astTag.getValueElement() == null || !astTag.getValueElement().getText().equals("node")) {
      return Optional.empty();
    }

    return Optional.of(refClass);
  }

}
