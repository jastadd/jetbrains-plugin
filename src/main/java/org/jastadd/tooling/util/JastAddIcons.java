package org.jastadd.tooling.util;

import com.intellij.icons.AllIcons;
import com.intellij.openapi.util.IconLoader;

import javax.swing.*;

public class JastAddIcons {

  public static final Icon FILE = IconLoader.getIcon("/META-INF/pluginIcon.svg", JastAddIcons.class);
  public static final Icon ASPECT = AllIcons.Actions.GroupByModuleGroup;
  public static final Icon ATTRIBUTE = IconLoader.getIcon("/META-INF/attributeIcon.svg", JastAddIcons.class);
  public static final Icon COL_ATTRIBUTE = ATTRIBUTE;
  public static final Icon INH_ATTRIBUTE = ATTRIBUTE;
  public static final Icon SYN_ATTRIBUTE = ATTRIBUTE;
  public static final Icon INTERTYPE_DECL = IconLoader.getIcon("/META-INF/intertypeIcon.svg", JastAddIcons.class);
  public static final Icon REWRITE = IconLoader.getIcon("/META-INF/rewriteIcon.svg", JastAddIcons.class);

  private JastAddIcons() {
    throw new IllegalStateException("Utility class");
  }

}
