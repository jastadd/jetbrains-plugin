package org.jastadd.tooling.common.psi;

import com.intellij.psi.PsiNameIdentifierOwner;

public interface NamedElement extends PsiNameIdentifierOwner {

}
