package org.jastadd.tooling.java;

import com.intellij.lexer.JavaHighlightingLexer;
import com.intellij.lexer.Lexer;
import com.intellij.openapi.editor.DefaultLanguageHighlighterColors;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighterBase;
import com.intellij.pom.java.LanguageLevel;
import com.intellij.psi.tree.IElementType;
import org.jetbrains.annotations.NotNull;

import static com.intellij.openapi.editor.colors.TextAttributesKey.createTextAttributesKey;

public class JavaSyntaxHighlighter extends SyntaxHighlighterBase {

  public static final TextAttributesKey NT_USE =
    createTextAttributesKey("NT_USE", DefaultLanguageHighlighterColors.HIGHLIGHTED_REFERENCE);
  public static final TextAttributesKey HIGHLEVEL_API_USE =
    createTextAttributesKey("HIGHLEVEL_API_USE", DefaultLanguageHighlighterColors.HIGHLIGHTED_REFERENCE);
  public static final TextAttributesKey LOWLEVEL_API_USE =
    createTextAttributesKey("LOWLEVEL_API_USE", DefaultLanguageHighlighterColors.HIGHLIGHTED_REFERENCE);
  public static final TextAttributesKey INTERNAL_API_USE =
    createTextAttributesKey("INTERNAL_API_USE", DefaultLanguageHighlighterColors.HIGHLIGHTED_REFERENCE);
  public static final TextAttributesKey ATTRIBUTE_CALL =
    createTextAttributesKey("ATTRIBUTE_CALL", DefaultLanguageHighlighterColors.HIGHLIGHTED_REFERENCE);
  public static final TextAttributesKey INTERTYPE_DECL_USE =
    createTextAttributesKey("INTERTYPE_DECL_USE", DefaultLanguageHighlighterColors.HIGHLIGHTED_REFERENCE);

  private static final TextAttributesKey[] EMPTY_KEYS = new TextAttributesKey[0];

  @NotNull
  @Override
  public Lexer getHighlightingLexer() {
    return new JavaHighlightingLexer(LanguageLevel.HIGHEST);
  }

  @NotNull
  @Override
  public TextAttributesKey @NotNull [] getTokenHighlights(IElementType tokenType) {
    return EMPTY_KEYS;
  }

}
