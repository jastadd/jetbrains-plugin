package org.jastadd.tooling.java;

import com.intellij.codeInspection.ProblemHighlightType;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiMethod;
import com.intellij.psi.PsiMethodCallExpression;
import com.intellij.psi.javadoc.PsiDocComment;
import com.intellij.psi.javadoc.PsiDocTag;
import com.intellij.psi.javadoc.PsiDocTagValue;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.jastadd.tooling.java.GeneratedCodeUtil.getNavigationUrlFromFileLocation;


public class JavaMethodHighlighter implements Annotator {

  @Override
  public void annotate(@NotNull final PsiElement element, @NotNull AnnotationHolder holder) {

    if (!(element instanceof PsiMethodCallExpression)) {
      return;
    }
    PsiMethodCallExpression psiMethodCallExpression = (PsiMethodCallExpression) element;

    PsiMethod targetMethod = psiMethodCallExpression.resolveMethod();
    if (targetMethod == null) {
      return;
    }

    PsiElement referenceElement = psiMethodCallExpression.getMethodExpression().getReferenceNameElement();
    if (referenceElement == null) {
      return;
    }

    // for anything interesting, there is a DocComment with a declaredat tag
    PsiDocComment docComment = targetMethod.getDocComment();
    if (docComment == null) {
      return;
    }

    PsiDocTag declaredAtTag = docComment.findTagByName("declaredat");
    if (declaredAtTag == null) {
      return;
    }

    String declaredAtString = Arrays.stream(declaredAtTag.getDataElements()).map(PsiElement::getText).collect(Collectors.joining());

    // case 1: there is an AstNodeAnnotation
    // TODO check if ASTNodeAnnotation can be renamed
    if (Arrays.stream(targetMethod.getAnnotations()).anyMatch(x -> Objects.requireNonNull(x.getQualifiedName()).contains("ASTNodeAnnotation.Attribute"))) {
      // TODO add more info in tooltip
      holder.newAnnotation(HighlightSeverity.INFORMATION, "JastAdd attribute")
        .range(referenceElement.getTextRange())
        .highlightType(ProblemHighlightType.INFORMATION)
        .textAttributes(JavaSyntaxHighlighter.ATTRIBUTE_CALL)
        .tooltip("<b>JastAdd Attribute</b><br/>Definition: " + getNavigationUrlFromFileLocation(declaredAtString))
        .create();
    } else {
      // case 2: use the JastAdd DocComment
      TextAttributesKey highlightKey;
      String description;

      PsiDocTag apiLevelTag = docComment.findTagByName("apilevel");
      if (apiLevelTag == null) {
        highlightKey = JavaSyntaxHighlighter.INTERTYPE_DECL_USE;
        description = "JastAdd inter-type declaration";
      } else {
        PsiDocTagValue value = apiLevelTag.getValueElement();
        if (value == null) {
          return;
        }
        switch (value.getText()) {
          case "high": // the first value ends at the hyphen
            highlightKey = JavaSyntaxHighlighter.HIGHLEVEL_API_USE;
            description = "JastAdd high-level API";
            break;
          case "low": // the first value ends at the hyphen
            highlightKey = JavaSyntaxHighlighter.LOWLEVEL_API_USE;
            description = "JastAdd low-level API";
            break;
          case "internal":
            highlightKey = JavaSyntaxHighlighter.INTERNAL_API_USE;
            description = "JastAdd internal API";
            break;
          default:
            // ignore error case
            return;
        }
      }

      // TODO add more info in tooltip
      holder.newAnnotation(HighlightSeverity.INFORMATION, description)
        .range(referenceElement.getTextRange())
        .highlightType(ProblemHighlightType.INFORMATION)
        .textAttributes(highlightKey)
        .tooltip("<b>" + description + "</b><br/>Definition: " + getNavigationUrlFromFileLocation(declaredAtString))
        .create();
    }
  }
}
