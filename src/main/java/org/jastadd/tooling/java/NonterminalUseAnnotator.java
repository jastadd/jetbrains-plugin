package org.jastadd.tooling.java;


import com.intellij.codeInspection.ProblemHighlightType;
import com.intellij.lang.annotation.AnnotationHolder;
import com.intellij.lang.annotation.Annotator;
import com.intellij.lang.annotation.HighlightSeverity;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.javadoc.PsiDocComment;
import com.intellij.psi.javadoc.PsiDocTag;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

import static org.jastadd.tooling.grammar.GrammarUtil.asReferenceToTypeDecl;
import static org.jastadd.tooling.java.GeneratedCodeUtil.getNavigationUrlFromFileLocation;

public class NonterminalUseAnnotator implements Annotator {

  @Override
  public void annotate(@NotNull final PsiElement element, @NotNull AnnotationHolder holder) {

    Optional<PsiClass> classOptional = asReferenceToTypeDecl(element);

    if (classOptional.isPresent()) {
      PsiDocComment docComment = classOptional.get().getDocComment();
      assert docComment != null; // asReferenceToTypeDecl ensures this is not null

      PsiDocTag declaredAtTag = docComment.findTagByName("declaredat");
      if (declaredAtTag == null) {
        return;
      }
      String declaredAt = Arrays.stream(declaredAtTag.getDataElements()).map(PsiElement::getText).collect(Collectors.joining());

      PsiDocTag productionTag = docComment.findTagByName("astdecl");
      if (productionTag == null) {
        return;
      }
      String production = Arrays.stream(productionTag.getDataElements()).map(PsiElement::getText).collect(Collectors.joining());
      String reference = getNavigationUrlFromFileLocation(declaredAt);

      holder.newAnnotation(HighlightSeverity.INFORMATION, "JastAdd Nonterminal: " + production)
        .range(element.getTextRange())
        .highlightType(ProblemHighlightType.INFORMATION)
        .textAttributes(JavaSyntaxHighlighter.NT_USE)
        .tooltip("<b>JastAdd Nonterminal</b><br/>Production: <i>" + production + "</i><br/><i>Declared at </i>" + reference)
        .create();
    }
  }

}
