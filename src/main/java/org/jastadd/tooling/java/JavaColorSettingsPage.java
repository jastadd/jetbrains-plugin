package org.jastadd.tooling.java;

import com.intellij.ide.highlighter.JavaFileHighlighter;
import com.intellij.openapi.editor.colors.TextAttributesKey;
import com.intellij.openapi.fileTypes.SyntaxHighlighter;
import com.intellij.openapi.options.colors.AttributesDescriptor;
import com.intellij.openapi.options.colors.ColorDescriptor;
import com.intellij.openapi.options.colors.ColorSettingsPage;
import org.jastadd.tooling.util.JastAddIcons;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.swing.*;
import java.util.HashMap;
import java.util.Map;

public class JavaColorSettingsPage implements ColorSettingsPage {

  private static final AttributesDescriptor[] DESCRIPTORS = new AttributesDescriptor[]{
    new AttributesDescriptor("Nonterminal use", JavaSyntaxHighlighter.NT_USE),
    new AttributesDescriptor("High-Level API use", JavaSyntaxHighlighter.HIGHLEVEL_API_USE),
    new AttributesDescriptor("Low-Level API use", JavaSyntaxHighlighter.LOWLEVEL_API_USE),
    new AttributesDescriptor("Internal API use", JavaSyntaxHighlighter.INTERNAL_API_USE),
    new AttributesDescriptor("Attribute call", JavaSyntaxHighlighter.ATTRIBUTE_CALL),
    new AttributesDescriptor("Inter-Type declaration use", JavaSyntaxHighlighter.INTERTYPE_DECL_USE)
  };

  @Nullable
  @Override
  public Icon getIcon() {
    return JastAddIcons.FILE;
  }

  @NotNull
  @Override
  public SyntaxHighlighter getHighlighter() {
    return new JavaFileHighlighter();
  }

  @NotNull
  @Override
  public String getDemoText() {
    return "<NT>Marking</NT> m; // nonterminal\n" +
      "String name = m.<HAPI>getName()</HAPI>; // high-level API call\n" +
      "ASTNode child = m.<LAPI>getChild(0)</LAPI>; // low-level API call\n" +
      "boolean b = m.<IAPI>canRewrite()</IAPI>; // internal API call\n" +
      "m = petriNet.<ATT>initialMarking()</ATT>; // (nonterminal) attribute\n" +
      "m.<ITD>fire()</ITD>; // inter-type declaration call\n";
  }

  @Nullable
  @Override
  public Map<String, TextAttributesKey> getAdditionalHighlightingTagToDescriptorMap() {
    Map<String, TextAttributesKey> m = new HashMap<>();
    m.put("ATT", JavaSyntaxHighlighter.ATTRIBUTE_CALL);
    m.put("HAPI", JavaSyntaxHighlighter.HIGHLEVEL_API_USE);
    m.put("LAPI", JavaSyntaxHighlighter.LOWLEVEL_API_USE);
    m.put("IAPI", JavaSyntaxHighlighter.INTERNAL_API_USE);
    m.put("NT", JavaSyntaxHighlighter.NT_USE);
    m.put("ITD", JavaSyntaxHighlighter.INTERTYPE_DECL_USE);
    return m;
  }

  @NotNull
  @Override
  public AttributesDescriptor @NotNull [] getAttributeDescriptors() {
    return DESCRIPTORS;
  }

  @NotNull
  @Override
  public ColorDescriptor @NotNull [] getColorDescriptors() {
    return ColorDescriptor.EMPTY_ARRAY;
  }

  @NotNull
  @Override
  public String getDisplayName() {
    return "JastAdd Java Extension";
  }

}
