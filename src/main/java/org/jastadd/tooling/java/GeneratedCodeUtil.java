package org.jastadd.tooling.java;

import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.application.ex.ApplicationEx;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.vfs.LocalFileSystem;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.PsiElement;
import com.intellij.util.ui.EDT;
import org.jetbrains.annotations.NotNull;

import java.nio.file.Paths;

public class GeneratedCodeUtil {
  /**
   * Get the line of the PSI element within its document
   *
   * @param element a PSI element
   * @return the line (first line is 1, if no line could be determined, 0 is returned)
   */
  public static int getLine(@NotNull PsiElement element) {
    int line = 0;
    Document document = element.getContainingFile().getViewProvider().getDocument();
    if (document != null) {
      line = document.getLineNumber(element.getTextRange().getStartOffset() + 1);
    }
    return line;
  }

  public static String getNavigationUrlFromFileLocation(String jastAddFileLocation) {

    int pos = jastAddFileLocation.lastIndexOf(':');
    if (pos <= 0 || pos == jastAddFileLocation.length() - 1) {
      return jastAddFileLocation;
    }

    String path = jastAddFileLocation.substring(0, pos);
    // the method performing the refresh is not allowed when the following condition is not met
    // see com.intellij.openapi.vfs.newvfs.RefreshQueueImpl:execute
    VirtualFile vFile = ((ApplicationEx) ApplicationManager.getApplication()).holdsReadLock() || EDT.isCurrentThreadEdt()
      ? LocalFileSystem.getInstance().findFileByPath(path)
      : LocalFileSystem.getInstance().refreshAndFindFileByPath(path);

    int line;
    try {
      line = Integer.parseInt(jastAddFileLocation.substring(pos + 1).trim());
    } catch (NumberFormatException e) {
      line = 1;
    }
    if (vFile == null) {
      return Paths.get(path).getFileName() + (line == 0 ? "" : ":" + line);
    }

    Document document = FileDocumentManager.getInstance().getDocument(vFile);
    if (document == null) {
      return vFile.getName() + (line == 0 ? "" : ":" + line);
    }
    int offset = document.getLineStartOffset(line - 1); // lines start with 0 in jetbrains

    return "<a href=\"#navigation/" + path + ":" + offset + "\">" + vFile.getName() + ":" + line + "</a>";
  }
}
