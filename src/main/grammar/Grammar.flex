// Copyright 2000-2020 JetBrains s.r.o. and other contributors. Use of this source code is governed by the Apache 2.0 license that can be found in the LICENSE file.
package org.jastadd.tooling.grammar.lexer;

import com.intellij.lexer.FlexLexer;
import com.intellij.psi.tree.IElementType;
import org.jastadd.tooling.grammar.parser.GrammarTypes;
import com.intellij.psi.TokenType;

%%

%class GrammarLexer
%implements FlexLexer
%unicode
%function advance
%type IElementType
%eof{
%eof}


WhiteSpace        = [ ] | \t | \f | \n | \r | \r\n
ID                = [:jletter:] [:jletterdigit:]*
MultiLineComment  = [/][*][^*]+[*]+([^*/][^*]*[*]+)*[/]
DocComment        = [/][*][*][^*]*[*]+([^*/][^*]*[*]+)*[/]
SingleLineComment = [/][/] [^\n\r]* (\n | \r | \r\n)


%xstate COMMENT
%state DECLARATION

%%

<DECLARATION> {
  {WhiteSpace}          { return TokenType.WHITE_SPACE; } // maybe ignore this
  {MultiLineComment}    { return GrammarTypes.MULTILINECOMMENT; } // maybe ignore this
  {DocComment}          { return GrammarTypes.DOCCOMMENT; } // maybe ignore this
  {SingleLineComment}   { return GrammarTypes.SINGLELINECOMMENT; } // maybe ignore this
}

<YYINITIAL,COMMENT> {
  {WhiteSpace}+         { yybegin(YYINITIAL); return TokenType.WHITE_SPACE; }
  {MultiLineComment}    { yybegin(YYINITIAL); return GrammarTypes.MULTILINECOMMENT; }
  {DocComment}          { yybegin(YYINITIAL); return GrammarTypes.DOCCOMMENT; }
  {SingleLineComment}   { yybegin(YYINITIAL); return GrammarTypes.SINGLELINECOMMENT; }
}

<YYINITIAL,DECLARATION> {
  "abstract"            { yybegin(DECLARATION); return GrammarTypes.ABSTRACT; }
  "rel"                 { yybegin(DECLARATION); return GrammarTypes.REL; }
  ";"                   { yybegin(COMMENT);     return GrammarTypes.SCOL; }
  ":"                   { yybegin(DECLARATION); return GrammarTypes.COL; }
  "::="                 { yybegin(DECLARATION); return GrammarTypes.ASSIGN; }
  "*"                   { yybegin(DECLARATION); return GrammarTypes.STAR; }
  "."                   { yybegin(DECLARATION); return GrammarTypes.DOT; }
  ","                   { yybegin(DECLARATION); return GrammarTypes.COMMA; }
  "<"                   { yybegin(DECLARATION); return GrammarTypes.LT; }
  ">"                   { yybegin(DECLARATION); return GrammarTypes.GT; }
  "["                   { yybegin(DECLARATION); return GrammarTypes.LBRACKET; }
  "]"                   { yybegin(DECLARATION); return GrammarTypes.RBRACKET; }
  "/"                   { yybegin(DECLARATION); return GrammarTypes.SLASH; }
  "?"                   { yybegin(DECLARATION); return GrammarTypes.QUESTION_MARK; }
  "->"                  { yybegin(DECLARATION); return GrammarTypes.RIGHT; }
  "<-"                  { yybegin(DECLARATION); return GrammarTypes.LEFT; }
  "<->"                 { yybegin(DECLARATION); return GrammarTypes.BIDIRECTIONAL; }
  {ID}                  { yybegin(DECLARATION); return GrammarTypes.ID; }
  [^]                   { return TokenType.BAD_CHARACTER; }
}
