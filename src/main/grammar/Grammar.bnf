/**
 * The rules in this file are based on the JavaCC specifications from the file
 * Jrag.jjt which is part of the JastAdd source code and can be found at
 * https://bitbucket.org/jastadd/jastadd2/src/1a3b0a9/src/javacc/jrag/Jrag.jjt
 * That file itself is based on the Java1.1.jjt file from the VTransformer example
 * from JavaCC by Sun Microsystems, Inc. Modifications have been made by the
 * JastAdd team to add support for JastAdd features.  The original author of
 * this file, according to older releases of JavaCC, was Sriram Sankar. A date
 * tag in the older releases shows the file was created on 3/5/97.
 */

{
  parserClass="org.jastadd.tooling.grammar.parser.GrammarParser"

  extends="com.intellij.extapi.psi.ASTWrapperPsiElement"

  psiClassPrefix="Grammar"
  psiImplClassSuffix="Impl"
  psiPackage="org.jastadd.tooling.grammar.psi"
  psiImplPackage="org.jastadd.tooling.grammar.psi.impl"

  elementTypeHolderClass="org.jastadd.tooling.grammar.parser.GrammarTypes"
  elementTypeClass="org.jastadd.tooling.grammar.psi.GrammarElementType"
  tokenTypeClass="org.jastadd.tooling.grammar.psi.GrammarTokenType"

  psiImplUtilClass="org.jastadd.tooling.grammar.psi.impl.GrammarPsiImplUtil"
}

GrammarFile ::= comment* ((type_decl | relation) comment*)*

comment ::= (WHITESPACE | MULTILINECOMMENT | DOCCOMMENT | SINGLELINECOMMENT)

type_decl ::= ABSTRACT? type_name (COL type_name)? (ASSIGN (component | nta_component)*)? SCOL
{
  extends="org.jastadd.tooling.grammar.psi.impl.GrammarTypeDeclImplExtension"
  implements="org.jastadd.tooling.common.psi.NamedElement"
}

nta_component ::= SLASH component SLASH

component ::= (component_name COL type_name STAR?) | (type_name STAR?) | (LBRACKET component_name COL type_name RBRACKET) | (LBRACKET type_name RBRACKET) | (LT component_name (COL (java_type_use))? GT)

java_type_use ::=  parameterized_java_type_use | simple_java_type_use

parameterized_java_type_use ::= simple_java_type_use LT java_type_use (COMMA java_type_use)* GT

simple_java_type_use ::=  java_name (DOT java_name)*

relation ::=  REL ((unnamed_role LEFT navigable_role) | (navigable_role RIGHT unnamed_role) | (navigable_role BIDIRECTIONAL navigable_role)) SCOL

unnamed_role ::= type_name | navigable_role

navigable_role ::= type_name DOT component_name (STAR | QUESTION_MARK)?

//// for auto-completion, it is helpful if we can distinguish the different IDs
//type_name ::= ID

type_name ::= ID
{
  extends="org.jastadd.tooling.grammar.psi.impl.GrammarTypeNameImplExtension"
  implements="org.jastadd.tooling.common.psi.NamedElement"
}

component_name ::= ID
{
  extends="org.jastadd.tooling.grammar.psi.impl.GrammarComponentNameImplExtension"
  implements="org.jastadd.tooling.common.psi.NamedElement"
}


java_name ::= ID
