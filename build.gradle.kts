import io.gitlab.arturbosch.detekt.Detekt
import org.jetbrains.changelog.Changelog
import org.jetbrains.changelog.markdownToHTML
import org.jetbrains.grammarkit.tasks.GenerateLexerTask
import org.jetbrains.grammarkit.tasks.GenerateParserTask

fun properties(key: String) = project.findProperty(key).toString()

plugins {

    id("idea")
    // Java support
    id("java")
    // Kotlin support - read more: https://plugins.gradle.org/plugin/org.jetbrains.kotlin.jvm
    id("org.jetbrains.kotlin.jvm") version "2.0.10"
    // gradle-intellij-plugin - read more: https://github.com/JetBrains/gradle-intellij-plugin
    id("org.jetbrains.intellij.platform") version "2.0.0"
    // gradle-changelog-plugin - read more: https://github.com/JetBrains/gradle-changelog-plugin
    id("org.jetbrains.changelog") version "2.2.1"
    // detekt linter - read more: https://detekt.github.io/detekt/gradle.html
    id("io.gitlab.arturbosch.detekt") version "1.23.3"
    // ktlint linter - read more: https://github.com/JLLeitschuh/ktlint-gradle
    id("org.jlleitschuh.gradle.ktlint") version "12.1.1"
    // grammrkit - read more: https://plugins.gradle.org/plugin/org.jetbrains.grammarkit
    id("org.jetbrains.grammarkit") version "2022.3.2.2"
}

group = properties("pluginGroup")
version = properties("pluginVersion")

// Configure project's dependencies
repositories {
    mavenCentral()
    intellijPlatform {
        defaultRepositories()
    }
}

java {
    sourceCompatibility = JavaVersion.VERSION_21
    targetCompatibility = JavaVersion.VERSION_21
}

idea {
    module {
        generatedSourceDirs.add(file("./src/gen"))
    }
}

// Dependencies are managed with Gradle version catalog - read more: https://docs.gradle.org/current/userguide/platforms.html#sub:version-catalog
dependencies {

    // IntelliJ Platform Gradle Plugin Dependencies Extension - read more: https://plugins.jetbrains.com/docs/intellij/tools-intellij-platform-gradle-plugin-dependencies-extension.html
    intellijPlatform {
        create(providers.gradleProperty("platformType"), providers.gradleProperty("platformVersion"))

        // Plugin Dependencies. Uses `platformBundledPlugins` property from the gradle.properties file for bundled IntelliJ Platform plugins.
        bundledPlugins(providers.gradleProperty("platformBundledPlugins").map { it.split(',') })

        // Plugin Dependencies. Uses `platformPlugins` property from the gradle.properties file for plugin from JetBrains Marketplace.
        plugins(providers.gradleProperty("platformPlugins").map { it.split(',') })

        instrumentationTools()
        pluginVerifier()
    }
}

// Configure gradle-intellij-plugin plugin.
// Read more: https://github.com/JetBrains/gradle-intellij-plugin
intellijPlatform {
    projectName = project.name

    pluginConfiguration {
        id = providers.gradleProperty("pluginId")
        name = providers.gradleProperty("pluginName")
        version = providers.gradleProperty("platformVersion")

        // Extract the <!-- Plugin description --> section from README.md and provide for the plugin's manifest
        description = providers.fileContents(layout.projectDirectory.file("README.md")).asText.map {
            val start = "<!-- Plugin description -->"
            val end = "<!-- Plugin description end -->"

            with(it.lines()) {
                if (!containsAll(listOf(start, end))) {
                    throw GradleException("Plugin description section not found in README.md:\n$start ... $end")
                }
                subList(indexOf(start) + 1, indexOf(end)).joinToString("\n").let(::markdownToHTML)
            }
        }

        val changelog = project.changelog // local variable for configuration cache compatibility
        // Get the latest available change notes from the changelog file
        changeNotes = providers.gradleProperty("pluginVersion").map { pluginVersion ->
            with(changelog) {
                renderItem(
                    (getOrNull(pluginVersion) ?: getUnreleased())
                        .withHeader(false)
                        .withEmptySections(false),
                    Changelog.OutputType.HTML,
                )
            }
        }

//        productDescriptor {
//            // ...
//        }
        ideaVersion {
            sinceBuild = providers.gradleProperty("pluginSinceBuild")
            untilBuild = providers.gradleProperty("pluginUntilBuild")
        }
        vendor {
            name = "Smarobix"
            email = "johannes@smarobix.com"
            url = "smarobix.com"
        }
    }

    pluginVerification {
        ides {
            recommended()
        }
    }
}

sourceSets {
    main {
        java.srcDir("src/gen/java")
    }
}

// Configure gradle-changelog-plugin plugin.
// Read more: https://github.com/JetBrains/gradle-changelog-plugin
changelog {
    version.set(properties("pluginVersion"))
    groups.set(listOf("Added", "Changed", "Deprecated", "Removed", "Fixed"))
    repositoryUrl.set(properties("pluginRepositoryUrl"))
}

// Configure detekt plugin.
// Read more: https://detekt.github.io/detekt/kotlindsl.html
detekt {
    source.from(files("build.gradle.kts"))
}

tasks {

    val generateGrammarLexer = task<GenerateLexerTask>("generateGrammarLexer") {
        sourceFile.set(file("src/main/grammar/Grammar.flex"))
        targetOutputDir.set(file("src/gen/java/org/jastadd/tooling/grammar/lexer/"))
        purgeOldFiles.set(true)
    }

    // not fully working because of https://github.com/JetBrains/gradle-grammar-kit-plugin/issues/3
    val generateGrammarParser = task<GenerateParserTask>("generateGrammarParser") {
        sourceFile.set(file("src/main/grammar/Grammar.bnf"))
        targetRootOutputDir.set(file("src/gen/java"))
        pathToParser.set("/org/jastadd/tooling/grammar/GrammarParser.java")
        pathToPsiRoot.set("/org/jastadd/tooling/grammar/psi")
        purgeOldFiles.set(true)
    }

    val generateAspectLexer = task<GenerateLexerTask>("generateAspectLexer") {
        sourceFile.set(file("src/main/grammar/Aspect.flex"))
        targetOutputDir.set(file("src/gen/java/org/jastadd/tooling/aspect/lexer/"))
        purgeOldFiles.set(true)
    }

    // not fully working because of https://github.com/JetBrains/gradle-grammar-kit-plugin/issues/3
    val generateAspectParser = task<GenerateParserTask>("generateAspectParser") {
        sourceFile.set(file("src/main/grammar/Aspect.bnf"))
        targetRootOutputDir.set(file("src/gen/java"))
        pathToParser.set("/org/jastadd/tooling/aspect/AspectParser.java")
        pathToPsiRoot.set("/org/jastadd/tooling/aspect/psi")
        purgeOldFiles.set(true)
    }

    compileJava {
        dependsOn(generateGrammarLexer)
        dependsOn(generateGrammarParser)
        dependsOn(generateAspectLexer)
        dependsOn(generateAspectParser)
    }

    withType<JavaCompile> {
        sourceCompatibility = "17"
        targetCompatibility = "17"
    }

    withType<Detekt> {
        jvmTarget = "17"
    }
}
