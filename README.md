# JastAdd Language Support

<!-- Plugin description -->
This plugin provides support for [JastAdd](https://jastadd.cs.lth.se/) and [Relational RAGs](https://jastadd.pages.st.inf.tu-dresden.de/relational-rags/)
<!-- Plugin description end -->
